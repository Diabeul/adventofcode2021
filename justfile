# Help message for all commands
help:
    @just --list

# Command to watch Java files and automatically compile and run the project
watch-run:
    @echo "Watching Java files for changes..."
    find src/main -name "*.java" | entr mvn exec:java -o -q
