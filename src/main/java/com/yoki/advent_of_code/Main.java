package com.yoki.advent_of_code;

import com.yoki.advent_of_code.aoc.AoC;
import lombok.SneakyThrows;

public class Main {

  @SneakyThrows
  public static void main(String[] args) {

    // AoC.builderToday()
    AoC.builder(2024, 3)
        .oneDayOnly()
        // .sampleInput("test.txt")
        .build().run();

    // n=10^7 O(n)
    // n=10^6 O(n * log(n))
    // n=10^4 O(n ^ n)

    // Permutations:
    // ---------
    // all permutations -> n!
    // all combinations -> 2^n
    // circular permutations all unique (necklace) -> (n-1)!
    // circular permutations all unique (bracelet) -> (n-1)!/2

    // Graph algos:
    // ---------

    // Travelling salesman problem
    // ---------------------
    // Greedy algo

    // Minimum Spanning Tree:
    // ---------------------
    // Kruskal (undirected) (weighted) (start from the shortest edge) (O(E lg E))
    // Prim (undirected) (weighted) (random first edge) (O(E lg V)

    // Shortest Path:
    // -------------
    // BFS (directed/undirected) (no weighted) (O(V+E))
    // Dijkstra (directed/undirected) (weighted) (not negative) (O(E + V Lg V))
    // Bellman-ford (directed/undirected) (weighted) (O(VE))
    // Topological Sort (directed) (acyclic) (weighted) (DAG) (O(V+E))

    // Context Free Grammar (X => XX, A => XA, ...)
    // -------------
    // CYK (Cocke-Younger-Kasami)
  }
}
