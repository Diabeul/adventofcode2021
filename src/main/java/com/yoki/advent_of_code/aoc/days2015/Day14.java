package com.yoki.advent_of_code.aoc.days2015;

import static java.lang.String.valueOf;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;

public class Day14 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day14(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    return valueOf(maxDistanceAfter(2503));
  }


  public String part2() {
    return valueOf(maxScoreAfter(2503));
  }

  private int maxScoreAfter(int time) {
    List<Reindeer> reindeer = this.input.lines().map(this::stringToReindeer).toList();

    for (int i = 0; i < time; i++) {
      reindeer.forEach(Reindeer::incrementDistance);
      int max = reindeer.stream().mapToInt(r -> r.traveledDistance).max().orElse(0);
      reindeer.stream().filter(r -> r.traveledDistance == max).forEach(r -> r.score++);
    }
    return reindeer.stream().mapToInt(r -> r.score).max().orElse(0);
  }

  private int maxDistanceAfter(int time) {
    return this.input.lines().map(this::stringToReindeer)
        .mapToInt(reindeer -> reindeer.distanceAfter(time)).max()
        .orElse(0);
  }

  private Reindeer stringToReindeer(String line) {
    String[] split = line.split(" ");
    return new Reindeer(Integer.parseInt(split[3]), Integer.parseInt(split[6]), Integer.parseInt(split[13]));
  }

  static class Reindeer {
    enum State {
      FLY,REST
    }
    private final int speed;
    private final int flyTime;
    private final int restTime;

    private State state = State.FLY;
    private int score = 0;
    private int timeTraveled = 0;
    private int traveledDistance = 0;

    public Reindeer(int speed, int flyTime, int restTime) {
      this.speed = speed;
      this.flyTime = flyTime;
      this.restTime = restTime;
    }

    public void incrementDistance() {
      switchState(timeTraveled);
      traveledDistance += calculateMovement();
      timeTraveled++;
    }

    public int distanceAfter(int time) {
      int res = 0;
      for (int i = 0; i < time; i++) {
        switchState(i);
        res += calculateMovement();
      }
      return res;
    }

    private void switchState(int i) {
      int sec = i %(flyTime+restTime);
      if (state == State.FLY && sec == flyTime) state = State.REST;
      else if (state == State.REST && sec == 0) state = State.FLY;
    }

    private int calculateMovement() {
      return switch (state) {
        case FLY -> speed;
        case REST -> 0;
      };
    }
  }
}