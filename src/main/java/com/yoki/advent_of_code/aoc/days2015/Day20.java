package com.yoki.advent_of_code.aoc.days2015;

import static java.lang.Math.sqrt;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import one.util.streamex.IntStreamEx;

public class Day20 extends AocDay {

  private final int totalGift;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day20(String input, PrintStream output) {
    super(input, output);
    this.totalGift = Integer.parseInt(this.input.trim());
  }

  public String part1() {
    int i;
    for (i = 0; sumOfDivisors(i) < totalGift; i++) {}
    return String.valueOf(i);
  }

  public String part2() {
    int i;
    for (i = 0; sumOfDivisorsLimit(i, 50) < totalGift; i++) {}
    return String.valueOf(i);
  }

  private long sumOfDivisorsLimit(int n, int limit) {
    long sum = 0;
    for (int i = 1; i <= sqrt(n); i++) {
      if (n % i == 0) {
        if (i <= limit) sum += n / i;
        if (n / i <= limit) sum += i;
      }
    }
    return sum * 11L;
  }

  private long sumOfDivisors(int n) {
    return IntStreamEx.rangeClosed(1, (int) sqrt(n))
        .filter(i -> n % i == 0)
        .map(i -> (i + n / i))
        .sum() * 10L;
  }
}