package com.yoki.advent_of_code.aoc.days2021;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

public class Day2 extends AocDay {

  private final List<Pair<String, Integer>> formattedInputs;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day2(String input, PrintStream output) {
    super(input, output);
    this.formattedInputs = this.input.lines().map(s -> s.split(" "))
        .map(s -> Pair.of(s[0], Integer.parseInt(s[1]))).toList();
  }

  public String part1() {
    int depth = 0;
    int position = 0;
    int aim = 0;
    for (var i : formattedInputs) {
      if (StringUtils.equals("down", i.getKey())) aim += i.getValue();
      else if (StringUtils.equals("up", i.getKey())) aim -= i.getValue();
      else position += i.getValue();
      depth += aim * i.getValue();
    }
    return String.valueOf(position * depth);
  }

  public String part2() {
    return part1();
  }

}
