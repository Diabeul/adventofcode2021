package com.yoki.advent_of_code.aoc.days2021;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.Arrays;

public class Day21 extends AocDay {

  private final int p1;
  private final int p2;
  private final int size;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day21(String input, PrintStream output) {
    super(input, output);
    String[] split = this.input.split("\n");
    this.p1 = Integer.parseInt(split[0].split(": ")[1]);
    this.p2 = Integer.parseInt(split[1].split(": ")[1]);
    this.size = 10;
  }

  public String part1() {
    Dice dice = new DeterministicDice(100);
    NormalBoard board = new NormalBoard(this.size, 1000, new Player(p1), new Player(p2), dice);
    return String.valueOf(board.play());
  }

  public String part2() {
    DiracBoard diracBoard = new DiracBoard(this.size, 21, p1, p2);
    return Arrays.toString(diracBoard.dplay());
  }

  abstract static class Board<T> {

    int size;
    int win;

    T p1;
    T p2;

    protected Board(int size, int win, T p1, T p2) {
      this.size = size;
      this.win = win;
      this.p1 = p1;
      this.p2 = p2;
    }

    public abstract int play();

    public int modPlus(int n) {
      return (n - 1) % size + 1;
    }
  }

  static class DiracBoard extends Board<Integer> {

    public DiracBoard(int size, int win, int p1, int p2) {
      super(size, win, p1, p2);
    }

    @Override
    public int play() {
      var res = dplay();
      return res[0].max(res[1]).intValue();
    }

    private BigInteger[] dplay() {
      return dplay(0, p2, 0, p1, 0, false);
    }

    private BigInteger[] dplay(int roll, int curPos, int curScore, int nextPos, int nextScore, boolean p1) {

      if (roll != 0) {
        curPos = modPlus(curPos + roll);
        curScore += curPos;
        if (curScore >= win) {
          return p1 ? new BigInteger[]{BigInteger.ONE, BigInteger.ZERO} : new BigInteger[]{BigInteger.ZERO, BigInteger.ONE};
        }
      }

      var res = new BigInteger[]{BigInteger.ZERO, BigInteger.ZERO};
      add(res, mul(dplay(3, nextPos, nextScore, curPos, curScore, !p1), 1));
      add(res, mul(dplay(4, nextPos, nextScore, curPos, curScore, !p1), 3));
      add(res, mul(dplay(5, nextPos, nextScore, curPos, curScore, !p1), 6));
      add(res, mul(dplay(6, nextPos, nextScore, curPos, curScore, !p1), 7));
      add(res, mul(dplay(7, nextPos, nextScore, curPos, curScore, !p1), 6));
      add(res, mul(dplay(8, nextPos, nextScore, curPos, curScore, !p1), 3));
      add(res, mul(dplay(9, nextPos, nextScore, curPos, curScore, !p1), 1));

      return res;
    }

    public void add(BigInteger[] a, BigInteger[] b) {
      for (int i = 0; i < a.length; i++) {
        a[i] = a[i].add(b[i]);
      }
    }

    public BigInteger[] mul(BigInteger[] a, int n) {
      BigInteger[] res = new BigInteger[a.length];
      for (int i = 0; i < a.length; i++) {
        res[i] = a[i].multiply(new BigInteger(String.valueOf(n)));
      }
      return res;
    }
  }


  static class NormalBoard extends Board<Player> {

    Dice dice;

    public NormalBoard(int size, int win, Player p1, Player p2, Dice dice) {
      super(size, win, p1, p2);
      this.dice = dice;
    }

    @Override
    public int play() {
      Player pc = p1;
      Player pn = p2;
      while (true) {
        int move = 0;
        for (int j = 0; j < 3; j++) {
          move += dice.roll();
        }
        if (pc.move(modPlus(move), win)) {
          return pn.score * dice.counter;
        }
        Player tp = pc;
        pc = pn;
        pn = tp;
      }
    }
  }

  static class Player {

    int score;
    int position;

    public Player(int position) {
      this.score = 0;
      this.position = position;
    }

    boolean move(int roll, int win) {
      position = roll;
      score += position;
      return this.win(win);
    }

    boolean win(int win) {
      return score >= win;
    }

    @Override
    public String toString() {
      return "Player{" +
          "score=" + score +
          ", position=" + position +
          '}';
    }
  }

  abstract static class Dice {

    int size;
    int counter = 0;

    protected Dice(int size) {
      this.size = size;
    }

    abstract int roll();
  }

  static class DeterministicDice extends Dice {

    int curr;

    public DeterministicDice(int size) {
      super(size);
      curr = 1;
    }

    @Override
    int roll() {
      counter++;
      if (curr > this.size) curr = 1;
      return curr++;
    }
  }
}
