package com.yoki.advent_of_code.aoc.days2021;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Optional.of;
import static java.util.stream.Collectors.toList;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.CollectionUtil;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.SneakyThrows;
import one.util.streamex.IntStreamEx;

public class Day22 extends AocDay {

  private final int n;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day22(String input, PrintStream output) {
    super(input, output);
    this.n = 50;
  }

  @SneakyThrows
  public String part1() {
    return String.valueOf(optiPart1(n));
  }

  @SneakyThrows
  public String part2() {
    var steps = loadRebootSteps();

    List<Integer> xGroup = new ArrayList<>();
    List<Integer> yGroup = new ArrayList<>();
    List<Integer> zGroup = new ArrayList<>();
    for (var step : steps) {
      xGroup.add(step.ranges[0][0]);
      xGroup.add(step.ranges[0][1]);
      yGroup.add(step.ranges[1][0]);
      yGroup.add(step.ranges[1][1]);
      zGroup.add(step.ranges[2][0]);
      zGroup.add(step.ranges[2][1]);
    }
    xGroup.sort(Integer::compareTo);
    yGroup.sort(Integer::compareTo);
    zGroup.sort(Integer::compareTo);

    int s = xGroup.size();

    boolean[][][] grid = fillGrid(steps, xGroup, yGroup, zGroup, s);

    long su = solution(xGroup, yGroup, zGroup, s, grid);

    return String.valueOf(su);
  }

  private long solution(List<Integer> xGroup, List<Integer> yGroup, List<Integer> zGroup, int s, boolean[][][] grid) {
    long su = 0L;
    for (int x = 0; x < s - 1; x++) {
      for (int y = 0; y < s - 1; y++) {
        for (int z = 0; z < s - 1; z++) {
          int val = grid[x][y][z] ? 1 : 0;
          su += (long) val * (xGroup.get(x + 1) - xGroup.get(x)) * (yGroup.get(y + 1) - yGroup.get(y)) * (
              zGroup.get(z + 1) - zGroup.get(z));
        }
      }
    }
    return su;
  }

  private boolean[][][] fillGrid(List<RebootStep> steps, List<Integer> xGroup, List<Integer> yGroup, List<Integer> zGroup,
      int s) {
    boolean[][][] grid = new boolean[s][s][s];
    for (var step : steps)
      for (int x = getIndex(xGroup, step.ranges[0][0]); x < getIndex(xGroup, step.ranges[0][1]); x++)
        for (int y = getIndex(yGroup, step.ranges[1][0]); y < getIndex(yGroup, step.ranges[1][1]); y++)
          for (int z = getIndex(zGroup, step.ranges[2][0]); z < getIndex(zGroup, step.ranges[2][1]); z++)
            grid[x][y][z] = step.switchOn;
    return grid;
  }

  private int optiPart1(int n) throws IOException {
    boolean[][][] a = new boolean[2 * n + 1][2 * n + 1][2 * n + 1];
    allSteps(n, a);

    int sum = 0;
    for (int x = 0; x <= n * 2; x++)
      for (int y = 0; y <= n * 2; y++)
        for (int z = 0; z <= n * 2; z++)
          sum += a[x][y][z] ? 1 : 0;

    return sum;
  }

  private void allSteps(int n, boolean[][][] a) throws IOException {
    List<RebootStep> steps = loadRebootSteps(n);
    for (var step : steps)
      for (int x = step.ranges[0][0]; x <= step.ranges[0][1]; x++)
        for (int y = step.ranges[1][0]; y <= step.ranges[1][1]; y++)
          for (int z = step.ranges[2][0]; z <= step.ranges[2][1]; z++)
            a[x + n][y + n][z + n] = step.switchOn;
  }

  private int getIndex(List<Integer> l, int c) {
    return lower(l, c);
  }

  private int lower(List<Integer> l, int key) {
    int lowerBound = 0;
    while (lowerBound < l.size()) {
      if (key > l.get(lowerBound)) {
        lowerBound++;
      } else {
        return lowerBound;
      }
    }
    return lowerBound;
  }

  private List<RebootStep> loadRebootSteps(int minMax) throws IOException {
    var ip = this.input.lines().toList();
    return ip.stream().map(s -> new RebootStep(s, minMax)).toList();
  }

  private List<RebootStep> loadRebootSteps() throws IOException {
    var ip = this.input.lines().toList();
    return ip.stream().map(RebootStep::new).toList();
  }

  public static class RebootStep {

    boolean switchOn;
    int[][] ranges;

    public RebootStep(String line, int minMax) {
      String[] split = line.split(" ");
      this.switchOn = split[0].equals("on");
      String[] rest = split[1].split(",");
      ranges = new int[3][2];
      for (int i = 0; i < 3; i++) {
        String[] range = rest[i].split("=")[1].split("\\.\\.");
        ranges[i] = new int[]{Math.max(Integer.parseInt(range[0]), -minMax), Math.min(Integer.parseInt(range[1]), minMax)};
      }
    }

    public RebootStep(String line) {
      String[] split = line.split(" ");
      this.switchOn = split[0].equals("on");
      String[] rest = split[1].split(",");
      ranges = new int[3][2];
      for (int i = 0; i < 3; i++) {
        String[] range = rest[i].split("=")[1].split("\\.\\.");
        ranges[i] = new int[]{Integer.parseInt(range[0]), Integer.parseInt(range[1])+1};
      }
    }
  }

  public static class CartesianProduct {
    private CartesianProduct() {}

    public static List<?> product(List<?> a, int n) {
      List<?>[] l = new List[n];
      for (int i = 0; i < n; i++) l[i] = a;
      return product(l);
    }

    public static List<?> product(List<?>... a) {
      if (a.length >= 2) {
        List<?> product = a[0];
        for (int i = 1; i < a.length; i++) {
          product = product(product, a[i]);
        }
        return CollectionUtil.flattenList((List<List>) product);
      }
      return emptyList();
    }

    private static <A, B> List<?> product(List<A> a, List<B> b) {
      return of(a.stream().map(e1 -> of(b.stream().map(e2 -> asList(e1, e2)).toList()).orElse(emptyList()))
          .flatMap(List::stream).toList()).orElse(emptyList());
    }
  }
}
