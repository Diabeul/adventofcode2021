package com.yoki.advent_of_code.aoc.days2022;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Day1 extends AocDay {

  private static final String EMPTY_ENTRY_REGEX = "\\n\\n";
  private static final int DEFAULT_MAX_CALORIES = -1;

  private final List<Integer> caloriesByElf;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day1(String input, PrintStream output) {
    super(input, output);
    this.caloriesByElf = getCalories(input);
  }

  public String part1() {
    return String.valueOf(caloriesByElf.stream()
        .mapToInt(Integer::intValue)
        .max()
        .orElse(DEFAULT_MAX_CALORIES)
    );
  }

  public String part2() {
    return String.valueOf(caloriesByElf.stream()
            .sorted(Comparator.reverseOrder())
            .limit(3)
            .mapToInt(Integer::intValue)
            .sum()
    );
  }

  private List<Integer> getCalories(String inventory) {
    return Arrays.stream(inventory.split(EMPTY_ENTRY_REGEX))
        .map(s -> s.lines().mapToInt(Integer::parseInt).sum())
        .toList();
  }
}
