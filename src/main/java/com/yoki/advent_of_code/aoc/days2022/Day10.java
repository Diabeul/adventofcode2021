package com.yoki.advent_of_code.aoc.days2022;

import static com.yoki.advent_of_code.utils.StringUtil.DARK;
import static com.yoki.advent_of_code.utils.StringUtil.LIGHT;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import lombok.Getter;
import one.util.streamex.IntStreamEx;

public class Day10 extends AocDay {

  private final List<Instruction> instructions;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day10(String input, PrintStream output) {
    super(input, output);
    instructions = this.input.lines().map(Instruction::parse).toList();
  }

  public String part1() {
    Tracker tracker = new CycleTracker();
    new CPU(tracker).execute(instructions);
    return tracker.res();
  }


  public String part2() {
    Tracker tracker = new CRT();
    new CPU(tracker).execute(instructions);
    return tracker.res();
  }

  enum Operation {
    noop(1),
    addx(2);

    int cycleTime;

    Operation(int cycleTime) {
      this.cycleTime = cycleTime;
    }
  }

  record Instruction(Operation op, int v) {

    static Instruction parse(String str) {
      String[] split = str.split(" ");
      int val = split.length > 1 ? Integer.parseInt(split[1]) : 0;
      return new Instruction(Operation.valueOf(split[0]), val);
    }
  }

  static class CPU {

    private final Tracker t;
    private int ra = 1;
    private int cycle = 0;

    public CPU(Tracker t) {
      this.t = t;
    }

    public void execute(List<Instruction> instructions) {
      instructions.forEach(this::execute);
    }

    public void execute(Instruction ins) {
      Operation op = ins.op;
      updateCycle(op);
      if (op == Operation.addx) {
        ra += ins.v;
      }
    }

    public void updateCycle(Operation op) {
      for (int i = 0; i < op.cycleTime; i++) {
        cycle++;
        t.track(ra, cycle);
      }
    }

    @Override
    public String toString() {
      return "CPU{" +
          "ra=" + ra +
          ", cycle=" + cycle +
          '}';
    }
  }

  interface Tracker {

    void track(int ra, int cycle);

    String res();
  }

  static class CRT implements Tracker {

    private static final int WIDTH = 40;
    private final StringBuilder res = new StringBuilder("\n");

    @Override
    public void track(int ra, int cycle) {
      var c = cycle % WIDTH;
      res.append(c >= ra && c <= ra + 2 ? LIGHT : DARK);
      if (cycle != 0 && c % WIDTH == 0)
        res.append("\n");
    }

    @Override
    public String res() {
      return res.toString();
    }
  }

  static class CycleTracker implements Tracker {

    private final List<Integer> cycleToCheck = IntStreamEx.rangeClosed(20, 220, 40).boxed().toList();

    @Getter
    private int sum = 0;

    @Override
    public void track(int ra, int cycle) {
      if (cycleToCheck.contains(cycle))
        this.sum += ra * cycle;
    }

    @Override
    public String res() {
      return String.valueOf(sum);
    }
  }
}
