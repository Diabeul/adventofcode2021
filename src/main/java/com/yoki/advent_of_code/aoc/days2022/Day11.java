package com.yoki.advent_of_code.aoc.days2022;

import static java.util.stream.Collectors.toList;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import one.util.streamex.StreamEx;

public class Day11 extends AocDay {

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day11(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    return String.valueOf(Monkeys.parse(this.input).playRoundWithReduc(20).res());
  }

  public String part2() {
    return String.valueOf(Monkeys.parse(this.input).playRoundWithoutReduc(10000).res());
  }

  record Operation(Op op, long val, boolean useOld) {
    enum Op {
      MUL("*"),
      DIV("/"),
      ADD("+"),
      SUB("-");

      private final String opChar;

      Op(String c) {
        this.opChar = c;
      }

      public static Op byChar(String c) {
        return Arrays.stream(values())
            .filter(o -> o.opChar.equals(c))
            .findFirst()
            .orElseThrow(() -> new RuntimeException("Operation not found !"));
      }
    }

    public long exec(Long old) {
      long with = useOld ? old : val;
      return switch (op) {
        case MUL -> old * with;
        case ADD -> old + with;
        case SUB -> old - with;
        case DIV -> old / with;
      };
    }

    static Operation parse(String input) {
      String[] old = input.split(" = ")[1].split("\\s+");
      boolean o = old[2].equals("old");
      long val = o ? 0L : Long.parseUnsignedLong(old[2]);
      return new Operation(Op.byChar(old[1]), val, o);
    }
  }

  record Test(int divisibleBy, int success, int fail) {

    public int test(long val) {
      return val % divisibleBy == 0 ? success : fail;
    }

    public static Test parse(String input) {
      String[] s = input.split("\n");
      int by = Integer.parseInt(s[0].split("by ")[1]);
      int suc = Integer.parseInt(s[1].split("monkey ")[1]);
      int fail = Integer.parseInt(s[2].split("monkey ")[1]);
      return new Test(by, suc, fail);
    }
  }

  static class Monkey {
    private List<Long> items;
    private final Operation operation;
    private final Test test;
    @Getter
    private long inspectionCounter = 0;

    public Monkey(List<Long> items, Operation operation, Test test) {
      this.items = items;
      this.operation = operation;
      this.test = test;
    }

    private void round(Monkeys monkeys, boolean reduceWorry) {
       for (var item: items) {
         long newValue = operation.exec(item);
         if (reduceWorry)
           newValue /= 3;
         else
           newValue %= monkeys.getMod();
         monkeys.addItemToMonkey(test.test(newValue), newValue);
         inspectionCounter++;
       }
       this.items = new ArrayList<>();
    }

    public void addItem(Long item) {
      this.items.add(item);
    }

    public int mod() {
      return this.test.divisibleBy();
    }

    static Monkey parse(String input) {
      List<Long> items = null;
      Operation op = null;
      for (String line : input.lines().toList()) {
        if (line.contains("Starting items"))
          items = Arrays.stream(line.split(": ")[1].split(", "))
              .map(Long::parseUnsignedLong).collect(toList());
        if (line.contains("Operation"))
          op = Operation.parse(line);
      }
      return new Monkey(items, op, Test.parse(input.split("Test: ")[1]));
    }
  }

  static class Monkeys {
    @Getter
    private final List<Monkey> monkeys;
    @Getter
    private final int mod;

    public Monkeys(List<Monkey> monkeys) {
      this.monkeys = monkeys;
      this.mod = monkeys.stream()
          .map(Monkey::mod)
          .reduce((a,b) -> a * b)
          .orElse(0);
    }

    static Monkeys parse(String input) {
      return new Monkeys(Arrays.stream(input.split("\n\n"))
          .map(Monkey::parse).toList());
    }


    public Monkeys playRoundWithReduc(int n) {
      playRound(n, true);
      return this;
    }

    public Monkeys playRoundWithoutReduc(int n) {
      playRound(n, false);
      return this;
    }

    private void playRound(int n, boolean reduceWorry) {
      for (int i = 0; i < n; i++)
        monkeys.forEach(m -> m.round(this, reduceWorry));
    }

    public void addItemToMonkey(int n, long item) {
      this.monkeys.get(n).addItem(item);
    }

    public long res() {
      return StreamEx.of(monkeys)
          .map(Monkey::getInspectionCounter)
          .reverseSorted()
          .limit(2)
          .reduce((a,b) -> a * b)
          .orElse(0L);
    }
  }
}
