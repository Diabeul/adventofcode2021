package com.yoki.advent_of_code.aoc.days2022;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.StringUtil;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

public class Day12 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day12(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    Grid grid = Grid.parse(this.input);
    return String.valueOf(grid.shortestPath());
  }

  public String part2() {
    Grid grid = Grid.parse(this.input);
    var res = Arrays.stream(grid.nodes)
        .filter(n -> n.value == 'a')
        .mapToInt(n -> grid.clone().shortestPath(n.number))
        .filter(i -> i != -1)
        .min().orElse(0);
    return String.valueOf(res);
  }

  @AllArgsConstructor
  static class Grid {

    private final int[][] map;
    private Node[] nodes;
    private Node origin;
    private Node end;

    public Grid(int[][] map) {
      this.map = map;
      final int sizeCol = map.length;
      final int sizeRow = map[0].length;
      nodes = new Node[sizeCol * sizeRow];
      for (int i = 0; i < sizeCol; i++) {
        for (int j = 0; j < sizeRow; j++) {
          int s = j + i * sizeRow;
          if (i + 1 < sizeCol)
            add(s, map[i][j], j + (i + 1) * sizeRow, map[i + 1][j]);

          if (j + 1 < sizeRow)
            add(s, map[i][j], j + 1 + i * sizeRow, map[i][j + 1]);

          if (i - 1 > 0)
            add(s, map[i][j], j + (i - 1) * sizeRow, map[i - 1][j]);

          if (j - 1 > 0)
            add(s, map[i][j], j - 1 + i * sizeRow, map[i][j - 1]);
        }
      }
    }

    private Node addNode(int n, int v) {
      if (nodes[n] == null)
        nodes[n] = new Node(n, v);

      if (v == 'S') {
        nodes[n].setValue('a');
        this.origin = nodes[n];
      } else if (v == 'E') {
        this.end = nodes[n];
        nodes[n].setValue('z');
      }

      return nodes[n];
    }

    private void add(int n, int a, int m, int b) {
      Node nodeA = addNode(n, a);
      Node nodeB = addNode(m, b);
      nodeA.addDest(nodeB);
      nodeB.addDest(nodeA);
    }

    public int shortestPath() {
      return shortestPath(origin.number);
    }

    private int shortestPath(int n) {
      Node origin = this.nodes[n];

      LinkedList<Node> q = new LinkedList<>();
      boolean[] visited = new boolean[this.nodes.length];

      visited[origin.number] = true;
      q.add(origin);
      origin.distance = 0;

      while (!q.isEmpty()) {
        Node cur = q.remove();
        for (var ad : cur.adjNode) {
          if (!visited[ad.number]) {
            visited[ad.number] = true;
            ad.distance = cur.distance + 1;
            ad.pred = cur;
            q.add(ad);

            if (ad == end) {
              return end.path();
            }
          }
        }
      }
      return -1;
    }

    public Grid clone() {
      return new Grid(this.map);
    }

    static Grid parse(String input) {
      return new Grid(input.lines()
          .map(StringUtil::stringToChars)
          .map(l -> l.stream().mapToInt(c -> (int) c).toArray())
          .toArray(int[][]::new));
    }
  }

  @Getter
  @Setter
  static class Node implements Comparator<Node> {

    private int number;
    private int value;
    private Node pred = null;
    private int distance = Integer.MAX_VALUE;
    private Set<Node> adjNode = new HashSet<>();

    public void addDest(Node des) {
      if (des.value - value < 2)
        adjNode.add(des);
    }

    public Node(int n, int value) {
      this.number = n;
      this.value = value;
    }

    public int path() {
      return path(0);
    }

    private int path(int c) {
      if (this.pred != null)
        return this.pred.path(c + 1);
      return c;
    }

    public Node clone() {
      return new Node(this.number, value);
    }

    @Override
    public String toString() {
      return "(" + number + ", " + (char) value + ')';
    }

    @Override
    public int compare(Node node, Node node1) {
      return Integer.compare(node.distance, node1.distance);
    }
  }
}
