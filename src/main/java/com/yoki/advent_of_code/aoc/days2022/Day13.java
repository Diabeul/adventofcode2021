package com.yoki.advent_of_code.aoc.days2022;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.apache.commons.lang3.tuple.Pair;

public class Day13 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day13(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    List<PacketPair> packets = Arrays.stream(this.input.split("\n\n"))
        .map(PacketPair::parse)
        .toList();

    int sum = 0;
    for (int i = 0; i < packets.size(); i++) {
      if (packets.get(i).rightOrder)
        sum += i + 1;
    }
    return String.valueOf(sum);
  }

  public String part2() {
    TreeSet<Packet> sortedPackets = new TreeSet<>(Arrays.stream(this.input.split("\n\n"))
        .map(m -> Arrays.stream(m.split("\n")).map(Packet::parse).toList())
        .flatMap(Collection::stream)
        .toList());

    Packet divider1 = Packet.parse("[[2]]");
    sortedPackets.add(divider1);

    Packet divider2 = Packet.parse("[[6]]");
    sortedPackets.add(divider2);

    int i = 0;
    int divider1Pos = 0;
    int divider2Pos = 0;
    while (!sortedPackets.isEmpty()) {
      Packet packet = sortedPackets.pollFirst();
      if (packet.equals(divider1))
        divider1Pos = i+1;
      if (packet.equals(divider2))
        divider2Pos = i+1;
      i++;
    }

    return String.valueOf(divider1Pos*divider2Pos);
  }

  record PacketPair(Pair<Packet, Packet> pair, boolean rightOrder) {

    static PacketPair parse(String in) {
      List<Packet> packets = Arrays.stream(in.split("\n"))
          .map(Packet::parse)
          .toList();
      assert packets.size() == 2;

      Pair<Packet, Packet> pair = Pair.of(packets.get(0), packets.get(1));
      boolean rightOrder = pair.getLeft().compareTo(pair.getRight()) < 0;
      return new PacketPair(pair, rightOrder);
    }
  }

  static class Packet implements Comparable<Packet> {

    private List<Packet> packets = new ArrayList<>();
    protected int val = 0;

    public Packet(List<Packet> items) {
      this.packets = items;
    }

    public Packet(int val) {
      this.val = val;
    }

    @Override
    public int compareTo(Packet packet) {
      if (packet instanceof PacketValue)
        return compareLists(packets, List.of(packet));

      if (this instanceof PacketValue)
        return compareLists(List.of(this), packet.packets);

      return compareLists(packets, packet.packets);
    }

    private int compareLists(List<Packet> packets, List<Packet> others) {
      int max = Math.max(others.size(), packets.size());
      for (int i = 0; i < max; i++) {
        if (packets.size() == i)
          return -1;
        if (others.size() == i)
          return 1;

        int res = packets.get(i).compareTo(others.get(i));
        if (res != 0)
          return res;
      }
      return 0;
    }

    static Packet parse(String input) {
      return parse(JsonParser.parseString(input).getAsJsonArray());
    }

    private static Packet parse(JsonArray array) {
      var items = new ArrayList<Packet>();
      array.forEach(e -> items.add(e.isJsonArray() ? parse(e.getAsJsonArray()) : new PacketValue(e.getAsInt())));
      return new Packet(items);
    }

    @Override
    public String toString() {
      String packetStrings = packets.stream().map(Packet::toString).collect(Collectors.joining(","));
      return "[" + packetStrings + "]";
    }
  }

  static class PacketValue extends Packet {

    public PacketValue(int value) {
      super(value);
    }

    @Override
    public int compareTo(Packet packet) {
      if (packet instanceof PacketValue value)
        return Integer.compare(this.val, value.val);
      return super.compareTo(packet);
    }

    @Override
    public String toString() {
      return this.val + "";
    }

  }
}
