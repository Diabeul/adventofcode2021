package com.yoki.advent_of_code.aoc.days2022;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Integer.MIN_VALUE;
import static java.lang.Integer.max;
import static java.lang.Integer.min;
import static java.lang.Integer.parseInt;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import one.util.streamex.IntStreamEx;

public class Day14 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day14(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    Solids solids = Solids.of(this.input);
    int i = solids.addSandsUntilAbyss();
    return String.valueOf(i);
  }

  public String part2() {
    Solids solids = Solids.of(this.input);
    int i = solids.addSandsUntilFloor();
    return String.valueOf(i);

  }

  static class Solids extends HashSet<Coord> {
    private int minY = MAX_VALUE;
    private int maxY = MIN_VALUE;
    private int maxX = MIN_VALUE;
    private int minX = MAX_VALUE;

    static Solids of(String input) {
      Solids coords = new Solids();
      input.lines().forEach(l -> coords
          .addLine(Arrays.stream(l.split(" -> "))
          .map(Coord::of)
          .toList())
      );
      return coords;
    }

    public void addIncludeBetween(Coord a, Coord b) {
      minY = min(minY, min(a.y, b.y));
      maxY = max(maxY, max(a.y, b.y));
      minX = min(minX, min(a.x, b.x));
      maxX = max(maxX, max(a.x, b.x));
      this.addAll(a.getStraightLine(b));
    }

    public void addLine(List<Coord> line) {
      for (int i = 1; i < line.size(); i++) {
        addIncludeBetween(line.get(i-1), line.get(i));
      }
    }

    public int addSandsUntilFloor() {
      int i = 0;
      while(this.addSandUntilFloor()) i++;
      return i;
    }

    public int addSandsUntilAbyss() {
      int i = 0;
      while(this.addSand()) i++;
      return i;
    }

    public boolean addSand() {
      Coord o = new Coord(500, 0);
      while (o.y < maxY) {
        if (!this.contains(o.down())) {
          o = o.down();
          continue;
        }

        if (!this.contains(o.downLeft())) {
          o = o.downLeft();
          continue;
        }

        if (!this.contains(o.downRight())) {
          o = o.downRight();
          continue;
        }

        this.add(new Coord(o.x, o.y));
        return true;
      }
      return false;
    }

    public boolean addSandUntilFloor() {
      Coord o = new Coord(500, 0);
      while (!this.contains(o)) {
        if (!this.contains(o.down()) && o.y < maxY+1) {
          o = o.down();
          continue;
        }

        if (!this.contains(o.downLeft()) && o.y < maxY+1) {
          o = o.downLeft();
          continue;
        }

        if (!this.contains(o.downRight()) && o.y < maxY+1) {
          o = o.downRight();
          continue;
        }

        this.add(new Coord(o.x, o.y));
        return true;
      }
      return false;
    }

    public String toString() {
      StringBuilder res = new StringBuilder();
      for (int y = minY-10; y <= maxY+10; y++) {
        StringBuilder sb = new StringBuilder();
        for (int x = minX-10; x <= maxX+10; x++) {
          if (this.contains(new Coord(x, y))) {
            sb.append("#");
          } else {
            sb.append(".");
          }
        }
        res.append(sb).append("\n");
      }
      return res.toString();
    }

  }

  record Coord(int x, int y) {

    static Coord of(String in) {
      String[] split = in.split(",");
      return new Coord(parseInt(split[0]), parseInt(split[1]));
    }

    public Set<Coord> getStraightLine(Coord other) {
      if (this.x == other.x)
        return getStraightLinePoints(this.y, other.y).stream()
            .map(p -> new Coord(this.x, p))
            .collect(Collectors.toSet());

      return getStraightLinePoints(this.x, other.x).stream()
          .map(p -> new Coord(p, this.y))
          .collect(Collectors.toSet());
    }

    public Set<Integer> getStraightLinePoints(int x, int y) {
      return IntStreamEx.rangeClosed(min(x,y),max(x,y)).boxed().toSet();
    }

    public Coord down() {
      return new Coord(x, y+1);
    }

    public Coord downLeft() {
      return new Coord(x-1, y+1);
    }

    public Coord downRight() {
      return new Coord(x+1, y+1);
    }

    @Override
    public String toString() {
      return "(" + x + "," + y + ")";
    }
  }
}
