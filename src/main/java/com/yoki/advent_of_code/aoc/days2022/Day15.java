package com.yoki.advent_of_code.aoc.days2022;

import static com.yoki.advent_of_code.utils.vector.Tuple.manhattanDistance;
import static java.lang.Long.parseLong;
import static org.apache.commons.math3.util.FastMath.abs;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.CollectionUtil;
import com.yoki.advent_of_code.utils.vector.Tuple;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Deque;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Data;
import one.util.streamex.IntStreamEx;

public class Day15 extends AocDay {

  private final DistressScanning distress;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day15(String input, PrintStream output) {
    super(input, output);
    distress = new DistressScanning(this.input, 4000000);
  }

  public String part1() {
    return String.valueOf(distress.getImpossibleBeaconsByLine(distress.getMax() / 2, true).size());
  }

  public String part2() {
    return String.valueOf((long) distress.tuningFrequency());
  }

  @Data
  static class DistressScanning {

    private List<SensorBeacon> sensorBeacons;
    private long max;

    public DistressScanning(String input, long max) {
      this.sensorBeacons = input.lines().map(SensorBeacon::of).toList();
      this.max = max;
    }

    public double tuningFrequency() {
      Tuple<Long> pos = getDistressBeacon();
      return 4000000 * pos.get(0) + pos.get(1);
    }

    public Set<Long> getImpossibleBeaconsByLine(long y, boolean removeKnowns) {
      return sensorBeacons.stream()
          .filter(s -> s.inRange(y))
          .map(s -> s.notPossiblePlaces(y, removeKnowns))
          .flatMap(Collection::stream)
          .collect(Collectors.toSet());
    }

    private Tuple<Long> getDistressBeacon() {
      Deque<Tuple<Tuple<Long>>> quadrantStack = CollectionUtil.asStack(new Tuple<>(new Tuple<>(0L, 0L), new Tuple<>(max, max)));
      while (!quadrantStack.isEmpty()) {
        Tuple<Tuple<Long>> pop = quadrantStack.pop();
        Tuple<Long> minV = pop.get(0);
        Tuple<Long> maxV = pop.get(1);

        if (minV.equals(maxV)) {
          if (sensorBeacons.stream().noneMatch(pair -> pair.inRange(minV)))
            return minV;
        } else {
          Tuple<Long> midV = new Tuple<>((minV.get(0) + maxV.get(0)) / 2, (minV.get(1) + maxV.get(1)) / 2);
          var quadrants = List.of(
              new Tuple<>(minV, midV),
              new Tuple<>(new Tuple<>(midV.get(0) + 1, minV.get(1)), new Tuple<>(maxV.get(0), midV.get(1))),
              new Tuple<>(new Tuple<>(minV.get(0), midV.get(1) + 1), new Tuple<>(midV.get(0), maxV.get(1))),
              new Tuple<>(new Tuple<>(midV.get(0) + 1, midV.get(1) + 1), maxV)
          );
          for (var quadrant : quadrants) {
            if (quadrant.get(0).get(0) > quadrant.get(1).get(0) || quadrant.get(0).get(1) > quadrant.get(1).get(1))
              continue;

            if (sensorBeacons.stream().allMatch(pair -> pair.possibleUnseenPoints(quadrant.get(0), quadrant.get(1))))
              quadrantStack.push(quadrant);
          }
        }
      }
      return null;
    }
  }

  record SensorBeacon(Tuple<Long> sensor, Tuple<Long> beacon, long dist) {

    public boolean inRange(Tuple<Long> b) {
      return manhattanDistance(sensor, b) <= dist;
    }

    public boolean inRange(long y) {
      return abs(sensor.get(1) - y) <= dist;
    }

    public Set<Long> notPossiblePlaces(long y, boolean removeKnowns) {
      long x = sensor.get(0);
      long d = dist - abs(sensor.get(1) - y);
      Set<Long> res = IntStreamEx.rangeClosed((int) (x - d), (int) (x + d))
          .boxed().mapToLong(Integer::longValue)
          .boxed().collect(Collectors.toSet());
      if (beacon.get(1) == y && removeKnowns)
        res.remove(beacon.get(0));
      return res;
    }

    public boolean possibleUnseenPoints(Tuple<Long> min, Tuple<Long> max) {
      var corners = List.of(
          new Tuple<>(min.get(0), min.get(1)),
          new Tuple<>(min.get(0), max.get(1)),
          new Tuple<>(max.get(0), min.get(1)),
          new Tuple<>(max.get(0), max.get(1))
      );

      var largestDistance = corners.stream()
          .mapToLong(corner -> manhattanDistance(corner, sensor))
          .max().orElseThrow();

      return largestDistance > dist;
    }

    static SensorBeacon of(String line) {
      String[] split = line.split(":");
      String[] sens = split[0].split(",");
      String[] bea = split[1].split(",");
      Tuple<Long> sensor = new Tuple<>(
          parseLong(sens[0].split("=")[1]),
          parseLong(sens[1].split("=")[1])
      );
      Tuple<Long> beacon = new Tuple<>(
          parseLong(bea[0].split("=")[1]),
          parseLong(bea[1].split("=")[1])
      );
      return new SensorBeacon(sensor, beacon, manhattanDistance(sensor, beacon));
    }
  }

}
