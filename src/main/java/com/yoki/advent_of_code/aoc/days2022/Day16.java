package com.yoki.advent_of_code.aoc.days2022;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day16 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day16(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    this.input.lines();
    return String.valueOf(0);
  }


  public String part2() {
    return String.valueOf(0);
  }

  record Valve(String name, int flowRate, List<Valve> tunnels) {
//    static Valve of(String line) {
//      Pattern pattern = Pattern.compile("Valve (\\w{2}) .* rate=(\\d*);");
//      String[] split = line.split(";");
//      Matcher matcher = pattern.matcher(split[0]);

//    }
  }

}
