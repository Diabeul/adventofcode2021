package com.yoki.advent_of_code.aoc.days2022;

import static java.lang.Integer.parseInt;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.StringUtil;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

public class Day5 extends AocDay {

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day5(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    var stacks = CrateStacks.toStacks(this.input);
    this.input.lines()
        .filter(s -> s.startsWith("move"))
        .map(Movement::move)
        .forEach(stacks::craneMove);
    return stacks.getMessage();
  }

  public String part2() {
    var stacks = CrateStacks.toStacks(this.input);
    this.input.lines()
        .filter(s -> s.startsWith("move"))
        .map(Movement::move)
        .forEach(stacks::craneMove9001);
    return stacks.getMessage();
  }


  @NoArgsConstructor
  static class CrateStacks {

    @Getter
    private final List<Deque<Character>> stacks = new ArrayList<>();

    static CrateStacks toStacks(String input) {
      List<List<Character>> cranes = input.lines()
          .takeWhile(StringUtils::isNotEmpty)
          .map(StringUtil::stringToChars)
          .toList();

      CrateStacks stacks = new CrateStacks();
      for (int i = cranes.size() - 1; i >= 0; i--) {
        var characters = cranes.get(i);
        if (i == cranes.size() - 1) {
          characters.stream().filter(Character::isDigit).forEach(s -> stacks.add(new ArrayDeque<>()));
        } else {
          for (int k = 1, j = 0; k < characters.size(); k += 4, j++) {
            Character e = characters.get(k);
            if (Character.isLetter(e))
              stacks.push(j, e);
          }
        }
      }
      return stacks;
    }

    private void add(Deque<Character> stack) {
      stacks.add(stack);
    }

    public void push(int stackNo, Character item) {
      stacks.get(stackNo).push(item);
    }

    public void craneMove9001(Movement m) {
      Deque<Character> tmp = new ArrayDeque<>();
      for (int i = 0; i < m.move; i++) {
        tmp.push(stacks.get(m.from - 1).pop());
      }
      while (!tmp.isEmpty()) {
        stacks.get(m.to - 1).push(tmp.pop());
      }
    }

    public void craneMove(Movement m) {
      for (int i = 0; i < m.move; i++)
        stacks.get(m.to - 1).push(stacks.get(m.from - 1).pop());
    }

    public String getMessage() {
      return stacks.stream()
          .map(Deque::pop)
          .map(Object::toString)
          .collect(Collectors.joining());
    }
  }

  record Movement(int to, int from, int move) {

    static Movement move(String t) {
      String[] s = t.split(" to ");
      int to = parseInt(s[1]);

      String[] f = s[0].split(" from ");
      int from = parseInt(f[1]);

      String[] m = f[0].split("move ");
      int move = parseInt(m[1]);

      return new Movement(to, from, move);
    }

  }
}
