package com.yoki.advent_of_code.aoc.days2022;

import static com.yoki.advent_of_code.utils.BenchmarkUtil.printBench;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.HashSet;

public class Day6 extends AocDay {

  private final String msg;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day6(String input, PrintStream output) {
    super(input, output);
    this.msg = this.input.strip();
  }

  public String part1() {
    return String.valueOf(charsBeforePacket(4));
  }

  public String part2() {
    printBench(input);
    return String.valueOf(charsBeforePacket(14));
  }

  private int charsBeforePacket(int size) {
    for (int i = 0, j = size; j < msg.length(); i++, j++) {
      if (allUniqueHashset(msg.substring(i, j)))
        return j;
    }
    return 0;
  }

  private boolean allUnique(String substring) {
    return substring.chars().distinct().count() == substring.length();
  }

  private boolean allUniqueHashset(String substring) {
    HashSet<Character> characters = new HashSet<>();
    for (int i = 0; i < substring.length(); i++)
      if (!characters.add(substring.charAt(i)))
        return false;
    return true;
  }


}
