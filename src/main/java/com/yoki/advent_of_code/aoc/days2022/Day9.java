package com.yoki.advent_of_code.aoc.days2022;

import static com.yoki.advent_of_code.utils.CollectionUtil.lastElem;
import static java.lang.Integer.compare;
import static java.lang.Integer.parseInt;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

public class Day9 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day9(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    Knot k = new Knot(2);
    this.input.lines().toList()
        .stream().map(Move::parse)
        .forEach(k::move);
    k.printTails(25);
    return String.valueOf(k.tails.size());
  }

  public String part2() {
    Knot k = new Knot(10);
    this.input.lines().toList()
        .stream().map(Move::parse)
        .forEach(k::move);
    k.printTails(25);
    return String.valueOf(k.tails.size());
  }

  record Move(Dir dir, int amount) {

    enum Dir {
      R, U, L, D
    }

    static Move parse(String input) {
      String[] dirAmount = input.split(" ");
      return new Move(Dir.valueOf(dirAmount[0]), parseInt(dirAmount[1]));
    }
  }

  @EqualsAndHashCode
  @AllArgsConstructor
  @NoArgsConstructor
  static class End {

    private int x;
    private int y;

    public End clone() {
      return new End(x, y);
    }

    public void move(Move.Dir dir) {
      switch (dir) {
        case R -> x++;
        case L -> x--;
        case U -> y++;
        case D -> y--;
      }
    }

    public int distance(End other) {
      int px = other.x - x;
      int py = other.y - y;
      return (int) Math.sqrt(px * px + py * py);
    }

    public boolean catchUp(End other) {
      if (distance(other) < 2) return false;
      x += compare(other.x - x, 0);
      y += compare(other.y - y, 0);
      return true;
    }

    @Override
    public String toString() {
      return "(" + x + ", " + y + ")";
    }
  }

  static class Knot {

    private final List<End> ends = new ArrayList<>();

    @Getter
    private final Set<End> tails = new HashSet<>();

    public Knot(int size) {
      for (int i = 0; i < size; i++) {
        ends.add(new End(0, 0));
      }
    }

    private boolean catchUpTail(int tailNo) {
      return this.ends.get(tailNo).catchUp(this.ends.get(tailNo - 1));
    }

    public void move(Move m) {
      for (int i = 0; i < m.amount(); i++) {
        ends.get(0).move(m.dir());
        for (int j = 1; j < ends.size(); j++) {
          if (!catchUpTail(j)) break;
        }
        tails.add(lastElem(ends).clone());
      }
    }

    public void printTails(int max) {
      for (int i = max; i >= -max; i--) {
        for (int j = -max; j <= max; j++) {
          if (tails.contains(new End(j, i)))
            System.out.print("#");
          else
            System.out.print(".");
        }
        System.out.println();
      }
    }
  }

}
