package com.yoki.advent_of_code.aoc.days2023;

import static java.util.stream.Collectors.toList;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;

public class Day1 extends AocDay {
  private static final Map<String, String> STR_TO_NUM = Map.of(
      "one", "o1e",
      "two", "t2o",
      "three", "t3e",
      "four", "f4r",
      "five", "f5e",
      "six", "s6x",
      "seven", "s7n",
      "eight", "e8t",
      "nine", "n9e");

  private final List<String> calibrations;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day1(String input, PrintStream output) {
    super(input, output);
    this.calibrations = input.lines().collect(toList());
  }

  public String part1() {
    return String.valueOf(this.calibrations.stream()
        .mapToInt(this::getNumberInTextLine)
        .sum());
  }

  public String part2() {
    return String.valueOf(this.calibrations.stream()
        .map(this::replaceAllSpelledNumbers)
        .mapToInt(this::getNumberInTextLine)
        .sum());
  }

  private String replaceAllSpelledNumbers(String line) {
    return STR_TO_NUM.entrySet().stream()
        .reduce(
            line,
            (currentLine, entry) -> currentLine.replaceAll(entry.getKey(), entry.getValue()),
            (currentLine, newLine) -> newLine
        );
  }

  private Integer getNumberInTextLine(String line) {
    List<String> nums = line.chars()
        .filter(Character::isDigit)
        .mapToObj(Character::toString)
        .toList();
    if (nums.isEmpty()) throw new RuntimeException("Something went wrong!");
    return Integer.parseInt(nums.get(0) + nums.get(nums.size() - 1));
  }
}
