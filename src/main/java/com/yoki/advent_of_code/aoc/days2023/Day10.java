package com.yoki.advent_of_code.aoc.days2023;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.FileUtil;
import com.yoki.advent_of_code.utils.StringUtil;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import lombok.SneakyThrows;
import org.apache.commons.lang3.tuple.Pair;

public class Day10 extends AocDay {

  private final char[][] pipes;

  protected static final int[][] NEIGHBOURS = {
      {-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}
  };

  // [north, east, south, west]
  private static final Map<Character, int[]> TILES = Map.of(
      '|', new int[]{1,0,1,0},
      'L', new int[]{1,1,0,0},
      'J', new int[]{1,0,0,1},
      '-', new int[]{0,1,0,1},
      '7', new int[]{0,0,1,1},
      'F', new int[]{0,1,1,0}
  );

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  @SneakyThrows
  public Day10(String input, PrintStream output) {
    super(input, output);
    this.pipes = StringUtil.loadMapChar(input);
  }

  public String part1() {
//    var startPos = this.startPos();
    return String.valueOf(0);
  }
  public String part2() {
    return String.valueOf(0);
  }

//  private Pair<Integer, Integer> next(Pair<Integer, Integer> cur) {
//    return Arrays.stream(NEIGHBOURS)
//        .map(n -> Pair.of(cur. + n[0], y + n[1]))
//        .filter(n -> this.getGear(n.getLeft(), n.getRight()))
//        .findAny();
//
//  }
//
//  private boolean getGear(Object left, Object right) {
//    return false;
//  }
//
//  private Pair<Integer, Integer> startPos() {
//    for (int i = 0; i < pipes.length; i++)
//      for (int j = 0; j < pipes[0].length; j++)
//        if (pipes[i][j] == 'S')
//          return Pair.of(i, j);
//
//    System.err.println("No S found");
//    return null;
//  }
}
