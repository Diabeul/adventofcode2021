package com.yoki.advent_of_code.aoc.days2023;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day2 extends AocDay {

  private final List<GameRecord> gameRecords;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day2(String input, PrintStream output) {
    super(input, output);
    this.gameRecords = input.lines()
        .map(GameRecord::new)
        .toList();
  }

  public String part1() {
    return String.valueOf(this.gameRecords.stream()
        .filter(g -> g.isPossible(12, 13, 14))
        .mapToInt(GameRecord::getGameNum)
        .sum());
  }


  public String part2() {
    return String.valueOf(this.gameRecords.stream()
        .mapToInt(GameRecord::power)
        .sum());
  }


  private static class GameRecord {
    private int gameNum;

    private int maxRed;
    private int maxGreen;
    private int maxBlue;

    public GameRecord(String gameRecordLine) {
      Matcher matcher = Pattern
          .compile("Game (\\d+): (.+)")
          .matcher(gameRecordLine);

      if (matcher.find()) {
        this.gameNum = Integer.parseInt(matcher.group(1));
        this.computeRecord(matcher.group(2));
      }
    }

    private void computeRecord(String recordLine) {
      Matcher matcher = Pattern
          .compile("(\\d+)\\s+(green|blue|red)")
          .matcher(recordLine);

      while (matcher.find()) {
        int count = Integer.parseInt(matcher.group(1));
        switch (matcher.group(2)) {
          case "red" -> this.maxRed = Math.max(this.maxRed, count);
          case "green" -> this.maxGreen = Math.max(this.maxGreen, count);
          case "blue" -> this.maxBlue = Math.max(this.maxBlue, count);
          default -> throw new IllegalStateException("Unexpected value: " + matcher.group(2));
        }
      }
    }

    public boolean isPossible(int maxRed, int maxGreen, int maxBlue) {
      return this.maxRed <= maxRed && this.maxGreen <= maxGreen && this.maxBlue <= maxBlue;
    }

    public int getGameNum() {
      return gameNum;
    }

    public int power() {
      return this.maxRed * this.maxBlue * this.maxGreen;
    }

    @Override
    public String toString() {
      return "GameRecord{" +
          "gameNum=" + gameNum +
          ", maxRed=" + maxRed +
          ", maxGreen=" + maxGreen +
          ", maxBlue=" + maxBlue +
          '}';
    }
  }

}
