package com.yoki.advent_of_code.aoc.days2023;

import static java.util.stream.Collectors.joining;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.CollectionUtil;
import com.yoki.advent_of_code.utils.data_structure.Counter;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.lang3.tuple.Pair;

public class Day3 extends AocDay {

  protected static final int[][] NEIGHBOURS = {
      {-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}
  };

  private final Engine engine;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day3(String input, PrintStream output) {
    super(input, output);
    engine = new Engine(input.lines().toList());
  }

  public String part1() {
    return String.valueOf(this.engine.partsNumbers().stream().mapToInt(i -> i).sum());
  }

  public String part2() {
    return String.valueOf(this.engine.gearsRatio());
  }

  private static class Engine {
    Integer[][] schematic;
    Character[][] parts;

    public Engine(List<String> lines) {
      this.parts = lines.stream().map(l ->
          l.chars()
              .mapToObj(c -> (char)c)
              .map(c -> isNotDigitOrDot(c) ? c : '.')
              .toArray(Character[]::new)
      ).toArray(Character[][]::new);

      this.schematic = lines.stream().map(l ->
          l.chars()
              .mapToObj(c -> Character.isDigit(c) ? Character.digit(c, 10) : -1)
              .toArray(Integer[]::new))
          .toArray(Integer[][]::new);
    }

    public int gearsRatio() {
      int consecutive = 0;
      int currentNumber = 0;
      Map<Pair<Integer, Integer>, List<Integer>> res = new HashMap<>();
      Optional<Pair<Integer, Integer>> currentGear = Optional.empty();
      for (int i = this.schematic.length-1; i >= 0; i--) {
        for (int j = this.schematic[0].length-1; j >= 0; j--) {
          if (this.schematic[i][j] != -1) {
            currentNumber += this.schematic[i][j] * Math.pow(10,consecutive);
            consecutive++;

            var gear = nextToGear(i, j);
            if (gear.isPresent()) currentGear = gear;
          } else {
            if (currentGear.isPresent()) {
              res.putIfAbsent(currentGear.get(), new ArrayList<>());
              int finalCurrentNumber = currentNumber;
              res.compute(currentGear.get(), (k,v) -> CollectionUtil.asList(v, finalCurrentNumber));
            }
            currentGear = Optional.empty();
            consecutive = 0;
            currentNumber = 0;
          }
        }
      }

      if (currentGear.isPresent()) {
        res.putIfAbsent(currentGear.get(), new ArrayList<>());
        int finalCurrentNumber = currentNumber;
        res.compute(currentGear.get(), (k,v) -> CollectionUtil.asList(v, finalCurrentNumber));
      }

      return res.values().stream()
          .filter(v -> v.size() == 2)
          .mapToInt(v -> v.get(0) * v.get(1))
          .sum();
    }

    public List<Integer> partsNumbers() {
      ArrayList<Integer> res = new ArrayList<>();

      int consecutive = 0;
      int currentNumber = 0;
      boolean validNumber = false;
      for (int i = this.schematic.length-1; i >= 0; i--) {
        for (int j = this.schematic[0].length-1; j >= 0; j--) {
          if (this.schematic[i][j] != -1) {
            currentNumber += this.schematic[i][j] * Math.pow(10,consecutive);
            consecutive++;
            if (isValid(i, j)) validNumber = true;
          } else {
            if (validNumber) res.add(currentNumber);
            validNumber = false;
            consecutive = 0;
            currentNumber = 0;
          }
        }
      }
      if (validNumber) res.add(currentNumber);
      return res;
    }

    private Optional<Pair<Integer, Integer>> nextToGear(int x, int y) {
      return Arrays.stream(NEIGHBOURS)
          .map(n -> Pair.of(x + n[0], y + n[1]))
          .filter(n -> this.getGear(n.getLeft(), n.getRight()))
          .findAny();
    }

    private boolean isValid(int x, int y) {
      return Arrays.stream(NEIGHBOURS)
          .anyMatch(n -> this.getMask(x + n[0], y + n[1]));
    }

    private boolean getMask(int i, int j) {
      return i >= 0 && i < parts.length && j >= 0 && j < parts[0].length && this.parts[i][j] != '.';
    }

    private boolean getGear(int i, int j) {
      return i >= 0 && i < parts.length && j >= 0 && j < parts[0].length && this.parts[i][j] == '*';
    }

    public void print() {
      System.out.println(Arrays.stream(parts).map(line ->
          Arrays.stream(line)
              .map(String::valueOf)
              .collect(joining()))
          .collect(joining("\n")));

      System.out.println("----");

      System.out.println(Arrays.stream(schematic).map(line ->
              Arrays.stream(line)
                  .map(String::valueOf)
                  .map(s -> s.equals("-1") ? "." : s)
                  .collect(joining()))
          .collect(joining("\n")));
    }
  }

  public static boolean isNotDigitOrDot(char ch) {
    return !Character.isDigit(ch) && ch != '.';
  }

}
