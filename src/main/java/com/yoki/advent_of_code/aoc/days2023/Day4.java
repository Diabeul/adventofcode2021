package com.yoki.advent_of_code.aoc.days2023;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.data_structure.Counter;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import lombok.Data;
import one.util.streamex.IntStreamEx;

public class Day4 extends AocDay {

  private final List<Card> cards;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day4(String input, PrintStream output) {
    super(input, output);
    cards = this.input.lines()
        .map(Card::new)
        .toList();
  }

  public String part1() {
    return String.valueOf(this.cards.stream()
        .mapToInt(Card::score)
        .sum());
  }

  public String part2() {
    Counter<Integer> cardsCounter = new Counter<>(this.cards.stream().map(Card::getNumber).toList());
    for (Card card : cards) {
      Long multiplier = cardsCounter.get(card.number);
      IntStreamEx.rangeClosed(1, (int) card.copiesWon())
          .forEach(c -> cardsCounter.increment(card.number + c, multiplier));
    }
    return String.valueOf(cardsCounter.sum());
  }

  @Data
  private static class Card {
    private int number;
    private List<Integer> winnings;
    private List<Integer> got;

    Card(String line) {
      Matcher matcher = Pattern
          .compile("Card\\s+(\\d+): (.+) \\| (.+)")
          .matcher(line);

      if (matcher.find()) {
        this.number = Integer.parseInt(matcher.group(1));
        this.winnings = parseNumbers(matcher.group(2));
        this.got = parseNumbers(matcher.group(3));
      }
    }

    private List<Integer> parseNumbers(String numberList) {
      return Arrays.stream(numberList.trim().split("\\s+"))
          .map(Integer::parseInt)
          .toList();
    }

    public long copiesWon() {
      return got.stream().filter(n -> winnings.contains(n)).count();
    }

    public int score() {
      return (int) Math.pow(2, copiesWon() - 1L);
    }
  }
}
