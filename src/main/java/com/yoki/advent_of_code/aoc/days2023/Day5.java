package com.yoki.advent_of_code.aoc.days2023;

import static com.yoki.advent_of_code.utils.CollectionUtil.asStack;
import static java.lang.Math.min;
import static java.lang.Math.max;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import one.util.streamex.IntStreamEx;
import org.apache.commons.lang3.tuple.Pair;

public class Day5 extends AocDay {

  private final List<Mapper> mappers;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day5(String input, PrintStream output) {
    super(input, output);
    this.mappers = extractMappers();
  }

  public String part1() {
    return String.valueOf(extractSeeds().stream().mapToLong(this::newSeed).min().orElse(0L));
  }

  public String part2() {
    return String.valueOf(extractPairSeeds().stream().mapToLong(this::newSeed).min().orElse(0L));
  }

  private List<Pair<Long, Long>> extractPairSeeds() {
    List<Long> seeds = extractSeeds();
    return IntStreamEx.range(0, seeds.size(), 2)
        .mapToObj(i -> Pair.of(seeds.get(i), seeds.get(i) + seeds.get(i + 1)))
        .toList();
  }

  private List<Long> extractSeeds() {
    Matcher matcher = Pattern.compile("seeds:\\s+([\\d\\s]++)").matcher(this.input);
    if (matcher.find()) {
      return Arrays.stream(matcher.group(1).split("\\s+"))
                .map(Long::parseLong)
                .toList();
    }
    return List.of();
  }

  private List<Mapper> extractMappers() {
    Matcher matcher = Pattern.compile("(?<=map:\\s)([\\d\\s]+)").matcher(this.input);
    List<Mapper> res = new ArrayList<>();
    while (matcher.find()) res.add(new Mapper(matcher.group(1)));
    return res;
  }

  private long newSeed(long seed) {
    for (var m : mappers) seed = m.map(seed);
    return seed;
  }

  private long newSeed(Pair<Long, Long> seed) {
    Deque<Pair<Long, Long>> seeds = asStack(seed);
    for (var m: mappers) {
      seeds = m.map(seeds);
    }
    return seeds.stream().mapToLong(Pair::getLeft).min().orElse(0);
  }

  static class Mapper {
    private final List<Convertor> convertors;

    public Mapper(String input) {
      String[] split = input.trim().split("\n");
      this.convertors = Arrays.stream(split)
          .map(Convertor::new).toList();
    }

    public long map(long value) {
      for (var c : convertors) {
        if (c.inRange(value)) {
          return c.convert(value);
        }
      }
      return value;
    }

    public Deque<Pair<Long, Long>> map(Deque<Pair<Long, Long>> seeds) {
      var newSeeds = new ArrayDeque<Pair<Long, Long>>();
      while (!seeds.isEmpty()) {
        Pair<Long, Long> pop = seeds.pop();
        var start = pop.getLeft();
        var end = pop.getRight();
        boolean mapped = false;
        for (var c: convertors) {
          var overlapStart = max(start, c.source);
          var overlapEnd = min(end, c.sourceEnd);
          if (overlapStart < overlapEnd) {
            newSeeds.push(Pair.of(overlapStart + c.delta(), overlapEnd + c.delta()));
            if (overlapStart > start) seeds.push(Pair.of(start, overlapStart));
            if (overlapEnd < end) seeds.push(Pair.of(overlapEnd, end));
            mapped = true;
            break;
          }
        }
        if (!mapped)
          newSeeds.push(pop);
      }
      return newSeeds;
    }
  }

  static class Convertor {
    private final Long dest;
    private final Long destEnd;
    private final Long source;
    private final Long sourceEnd;

    public Convertor(String input) {
      List<Long> values = Arrays.stream(input.trim().split("\\s+")).map(Long::parseLong).toList();
      this.dest = values.get(0);
      this.source = values.get(1);
      var len = values.get(2);
      this.sourceEnd = source + len;
      this.destEnd = dest + len;
    }

    public Long delta() {
      return dest - source;
    }

    public boolean inRange(long value) {
      return source <= value && value < sourceEnd;
    }

    public long convert(long value) {
      return value + delta();
    }

    @Override
    public String toString() {
      return "[" + source + ", " + sourceEnd + ") -> [" + dest + ", " + destEnd + ")";
    }
  }
}
