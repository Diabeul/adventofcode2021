package com.yoki.advent_of_code.aoc.days2023;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import one.util.streamex.StreamEx;

public class Day6 extends AocDay {

  private final List<Race> races;
  private Race onlyRace;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day6(String input, PrintStream output) {
    super(input, output);
    List<Long> times = findNumberList("Time");
    List<Long> distances = findNumberList("Distance");
    this.races = StreamEx.of(times).zipWith(distances.stream()).map(Race::new).toList();

    Long time = findLineMatching("Time: (.*)")
        .map(s -> s.replace(" ", ""))
        .map(Long::parseLong).orElse(0L);
    Long distance = findLineMatching("Distance: (.*)")
        .map(s -> s.replace(" ", ""))
        .map(Long::parseLong).orElse(0L);
    this.onlyRace = new Race(time, distance);
  }

  public String part1() {
    return String.valueOf(this.races.stream()
        .map(Race::winnings)
        .reduce(1L, (a, b) -> a * b));
  }


  public String part2() {
    return String.valueOf(this.onlyRace.winnings());
  }

  record Race(long time, long distance) {
    public Race(Entry<Long, Long> entry) {
      this(entry.getKey(), entry.getValue());
    }

    public long winnings() {
      long count = 0;
      for (long i = this.time / 2; i > 0; i--) {
        if (i * (this.time-i) <= this.distance) break;
        count++;
      }
      count *= 2;
      return this.time%2==0 ? count - 1 : count;
    }
  }

  // group?==1
  private Optional<String> findLineMatching(String regex) {
    return findLineMatching(regex, 1);
  }

  private Optional<String> findLineMatching(String regex, int group) {
    Matcher matcher = Pattern.compile(regex).matcher(input);
    if (matcher.find())
      return Optional.of(matcher.group(group).strip());
    return Optional.empty();
  }

  private List<Long> findNumberList(String startsWith) {
    return findLineMatching(startsWith + ": (.*)")
        .map(l -> Arrays.stream(l.split("\\s+")))
        .map(l -> l.map(Long::parseLong).toList())
        .orElse(List.of());
  }
}
