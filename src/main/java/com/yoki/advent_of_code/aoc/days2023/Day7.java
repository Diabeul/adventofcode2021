package com.yoki.advent_of_code.aoc.days2023;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.CollectionUtil;
import com.yoki.advent_of_code.utils.StringUtil;
import java.io.PrintStream;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day7 extends AocDay {

  private static final List<String> HANDS = List.of(
      "([\\w])\\1{1}", // Pair
      "([\\w])\\1.?([\\w])\\2",// 2 Pair
      "([\\w])\\1{2}", // Brelan
      "(([\\w])\\2{2}([\\w])\\3{1}|([\\w])\\4{1}([\\w])\\5{2})", // Full
      "([\\w])\\1{3}", // Square
      "([\\w])\\1{4}" // Poker
  );

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day7(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    return commonPart(new part1Comparator());
  }

  public String part2() {
    return commonPart(new part2Comparator());
  }

  private String commonPart(HandStringComparator comp) {
    var handBids = input.lines().map(l -> new HandBid(l, comp)).toList();
    return String.valueOf(CollectionUtil.enumerateStream(handBids.stream().sorted().toList(), 1)
        .mapToInt(e -> e.item().getBid() * e.index()).sum());
  }

  interface HandStringComparator {
    default int compareHandString(String hand1, String hand2, List<Character> cards) {
      for (int i = 0; i < 5; i++) {
        char c1 = hand1.charAt(i);
        char c2 = hand2.charAt(i);
        if (c1 != c2) {
          return Integer.compare(cards.indexOf(c1), cards.indexOf(c2));
        }
      }
      return 0;
    }

    int compareHandString(String hand1, String hand2);
    boolean handMatch(String reg, String hand);
  }

  static class part1Comparator implements HandStringComparator {
    private static final List<Character> CARDS = List.of('2','3','4','5','6','7','8','9','T','J','Q','K','A');

    @Override
    public int compareHandString(String hand1, String hand2) {
      return compareHandString(hand1, hand2, CARDS);
    }

    public boolean handMatch(String reg, String hand) {
      String order = StringUtil.order(hand);
      Matcher matcher = Pattern.compile(reg).matcher(order);
      return matcher.find();
    }
  }

  static class part2Comparator implements HandStringComparator {
    private static final List<Character> CARDS = List.of('J','2','3','4','5','6','7','8','9','T','Q','K','A');

    @Override
    public int compareHandString(String hand1, String hand2) {
      return compareHandString(hand1, hand2, CARDS);
    }

    public boolean handMatch(String reg, String hand) {
      String order = StringUtil.order(hand);
      if (!order.contains("J")) {
        return Pattern.compile(reg).matcher(order).find();
      }
      for (var c : CARDS) {
        if (Pattern.compile(reg).matcher(StringUtil.order(order.replace('J', c))).find())
          return true;
      }
      return false;
    }
  }

  static class HandBid implements Comparable<HandBid> {
    private final String hand;
    private final int bid;
    private final int rank;

    private final HandStringComparator comp;

    public HandBid(String line, HandStringComparator comparator) {
      this.comp = comparator;

      var split = line.split("\\s");
      this.hand = split[0];
      this.bid = Integer.parseInt(split[1]);
      this.rank = getHandRank() + 1;
    }

    private int getHandRank() {
      for (int i = HANDS.size() - 1; i >= 0; i--) {
        if (comp.handMatch(HANDS.get(i), this.hand)) {
          return i;
        }
      }
      return -1;
    }

    @Override
    public int compareTo(HandBid o) {
      int c = Integer.compare(this.rank, o.rank);
      return c == 0 ? comp.compareHandString(this.hand, o.hand) : c;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      HandBid handBid = (HandBid) o;
      return this.compareTo(handBid) == 0;
    }

    @Override
    public int hashCode() {
      return Objects.hash(hand, bid, rank, comp);
    }

    @Override
    public String toString() {
      return "[" + hand + ", " + bid + ", rank: " + rank + "]";
    }

    public int getBid() {
      return this.bid;
    }

  }
}
