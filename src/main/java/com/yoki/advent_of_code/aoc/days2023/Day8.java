package com.yoki.advent_of_code.aoc.days2023;

import static java.lang.Math.max;
import static java.lang.Math.min;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.StringUtil;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day8 extends AocDay {

  private final String instruction;
  private List<Node> nodes;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day8(String input, PrintStream output) {
    super(input, output);
    List<String> lines = input.lines().collect(Collectors.toList());
    this.instruction = lines.remove(0);
    lines.remove(0);
    this.nodes = lines.stream().map(Node::new).toList();
    for (var n : nodes) {
      n.left = nodes.stream().filter(l -> l.name.equals(n.left.name)).findFirst().get();
      n.right = nodes.stream().filter(l -> l.name.equals(n.right.name)).findFirst().get();
    }
  }

  public String part1() {
    int counter = 1;
    Node current = this.nodes.stream().filter(n -> n.name.equals("AAA")).findFirst().get();
    while (true) {
      for (var c : StringUtil.stringToChars(instruction)) {
        current = current.next(c);
        if (current.name.equals("ZZZ")) {
          return String.valueOf(counter);
        }
        counter++;
      }
    }
  }

  public String part2() {
    long counter = 1;
    List<Node> currents = this.nodes.stream().filter(n -> n.name.endsWith("A")).toList();
    List<Long> counters = new ArrayList<>();
    while (!currents.isEmpty()) {
      for (var c : StringUtil.stringToChars(instruction)) {
        currents = currents.stream().map(n -> n.next(c)).toList();
        var oldLen = currents.size();
        currents = currents.stream().filter(n -> !n.name.endsWith("Z")).toList();
        if (oldLen != currents.size()) {
          counters.add(counter);
        }
        counter++;
      }
    }
    return String.valueOf(counters.stream().reduce(1L, this::lcm));
  }

  private long lcm(long n1, long n2) {
    if (n1 == 0 || n2 == 0) {
        return 0;
    }
    var absN1 = Math.abs(n1);
    var absN2 = Math.abs(n2);
    var max = Math.max(absN1, absN2);
    var min = Math.min(absN1, absN2);
    var lcm = max;
    while (lcm % min != 0) {
        lcm += max;
    }
    return lcm;
}

  class Node {
    String name;
    Node left;
    Node right;

    public Node(String line) {
      Matcher m = Pattern.compile("([\\w]+) = \\(([\\w]+),\\s?([\\w]+)\\)").matcher(line);
      if (m.find()) {
        name = m.group(1);
        left = new Node(m.group(2));
        right = new Node(m.group(3));
      } else {
        name = line;
      }
    }

    public Node next(char ins) {
      return switch (ins) {
        case 'L' -> this.left;
        case 'R' -> this.right;
        default -> this;
      };
    }
  }
}
