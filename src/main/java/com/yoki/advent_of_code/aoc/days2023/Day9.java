package com.yoki.advent_of_code.aoc.days2023;

import static com.yoki.advent_of_code.utils.CollectionUtil.asList;
import static com.yoki.advent_of_code.utils.CollectionUtil.lastElem;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day9 extends AocDay {

  private final List<List<Integer>> inputs;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day9(String input, PrintStream output) {
    super(input, output);
    this.inputs = input.lines().map(l -> Arrays.stream(l.split("\\s+"))
        .map(Integer::parseInt).toList())
        .toList();
  }

  public String part1() {
    return String.valueOf(inputs.stream().mapToInt(this::nextValue).sum());
  }

  public String part2() {
    return String.valueOf(inputs.stream().mapToInt(this::prevValue).sum());
  }

  private int prevValue(List<Integer> ints) {
    List<List<Integer>> cycles = getCycles(ints);
    var newVal = 0;
    for (var c : cycles) {
      newVal = c.get(0) - newVal;
    }
    return newVal;
  }

  private int nextValue(List<Integer> ints) {
    List<List<Integer>> cycles = getCycles(ints);
    var newVal = 0;
    for (var c : cycles) {
      newVal += lastElem(c);
    }
    return newVal;
  }

  private List<List<Integer>> getCycles(List<Integer> ints) {
    List<List<Integer>> cycles = new ArrayList<>();
    cycles.add(ints);
    var cycle = difference(ints);
    while (!cycle.stream().allMatch(i -> i == 0)) {
      cycles.add(cycle);
      cycle = difference(cycle);
    }
    Collections.reverse(cycles);
    return cycles;
  }

  private List<Integer> difference(List<Integer> numbers) {
    return IntStream.range(1, numbers.size())
        .mapToObj(i -> numbers.get(i) - numbers.get(i - 1))
        .collect(Collectors.toList());
  }

}
