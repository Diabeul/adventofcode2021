package com.yoki.advent_of_code.aoc.days2024;

import static com.alecs.mininom.Combinators.separatedPair;
import static com.alecs.mininom.Combinators.terminated;
import static com.alecs.mininom.Parsers.endLine;
import static com.alecs.mininom.Parsers.number;
import static com.alecs.mininom.Parsers.whitespace;
import static java.util.Collections.sort;

import com.alecs.mininom.MiniNom.ParseResult;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.alecs.mininom.MiniNom.Pair;
import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.CollectionUtil;

public class Day1 extends AocDay {

  private final List<Integer> first;
  private final List<Integer> second;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day1(String input, PrintStream output) {
    super(input, output);

    var parsed = parse(input).get();

    this.first = parsed.first();
    this.second = parsed.second();

    sort(first);
    sort(second);
  }

  public String part1() {
    return String.valueOf(CollectionUtil
        .zip(first.stream(), second.stream(), (a, b) -> Math.abs(a - b))
        .mapToInt(a -> a)
        .sum());
  }

  public String part2() {
    var secondFrequency = second.stream().collect(Collectors.groupingBy(i -> i, Collectors.counting()));
    var res = first.stream()
        .filter(secondFrequency::containsKey)
        .mapToInt(i -> i * secondFrequency.get(i).intValue())
        .sum();
    return String.valueOf(res);
  }

  private ParseResult<String, Pair<List<Integer>, List<Integer>>> parse(String input) {
    return terminated(separatedPair(number(), whitespace(), number()), endLine().opt())
        .fold(new Pair<List<Integer>, List<Integer>>(new ArrayList<>(), new ArrayList<>()), (acc, a) -> {
          acc.first().add(a.first());
          acc.second().add(a.second());
          return acc;
        }).parse(input);

  }

}
