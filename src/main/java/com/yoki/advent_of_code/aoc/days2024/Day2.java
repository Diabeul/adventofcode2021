package com.yoki.advent_of_code.aoc.days2024;

import static com.alecs.mininom.Parsers.endLine;
import static com.alecs.mininom.Parsers.number;
import static com.alecs.mininom.Parsers.space1;
import static java.lang.Math.abs;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import java.util.stream.IntStream;

public class Day2 extends AocDay {

  private final List<Report> reports;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day2(String input, PrintStream output) {
    super(input, output);
    this.reports = number().manySeparatedBy(space1()).map(Report::new)
        .manySeparatedBy(endLine()).parse(input).get();
  }

  public String part1() {
    var res = reports.stream().filter(Report::isSafe).count();
    return String.valueOf(res);
  }

  public String part2() {
    return String.valueOf(reports.stream().filter(Report::isSafeWithoutOne).count());
  }

  private record Report(List<Integer> levels) {

    public boolean isSafe() {
      return isListSafe(this.levels);
    }

    public boolean isSafeWithoutOne() {
      return IntStream.range(0, levels.size())
          .mapToObj(i -> IntStream.range(0, levels.size())
              .filter(j -> j != i)
              .mapToObj(levels::get)
              .toList())
          .anyMatch(this::isListSafe);
    }

    private boolean isListSafe(List<Integer> levels) {
      Integer diff = null;
      Integer prev = levels.getFirst();
      for (var i = 1; i < levels.size(); i++) {
        var l = levels.get(i);
        var newDiff = prev - l;
        if (abs(newDiff) > 3 || abs(newDiff) < 1 || diff != null && diff * newDiff <= 0)
          return false;
        diff = newDiff;
        prev = l;
      }
      return true;
    }

    @Override
    public String toString() {
      return "Report{" +
          "levels=" + levels +
          "safe=" + isSafe() +
          '}';
    }
  }
}
