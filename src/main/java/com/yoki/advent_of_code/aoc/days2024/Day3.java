package com.yoki.advent_of_code.aoc.days2024;

import static com.alecs.mininom.Combinators.alt;
import static com.alecs.mininom.Combinators.delimited;
import static com.alecs.mininom.Combinators.separatedPair;
import static com.alecs.mininom.Parsers.anyChar;
import static com.alecs.mininom.Parsers.number;
import static com.alecs.mininom.Parsers.tag;

import com.alecs.mininom.MiniNom.Pair;
import com.alecs.mininom.MiniNom.Parser;
import java.io.PrintStream;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

import com.yoki.advent_of_code.aoc.AocDay;

public class Day3 extends AocDay {

  private boolean active = true;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day3(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    int sum = Pattern
        .compile("mul\\((\\d{1,3}),(\\d{1,3})\\)")
        .matcher(input)
        .results()
        .mapToInt(m -> Integer.parseInt(m.group(1)) * Integer.parseInt(m.group(2)))
        .sum();

    return String.valueOf(instructionParser().parse(input).get());
  }

  public String part2() {
    int sum = Pattern
        .compile("mul\\((\\d{1,3}),(\\d{1,3})\\)|do\\(\\)|don't\\(\\)")
        .matcher(input)
        .results()
        .mapToInt(this::mapFound)
        .sum();
    return String.valueOf(sum);
  }

  private sealed interface Instruction {
    record Mul(Integer left, Integer right) implements Instruction {
    }

    record Do() implements Instruction {
    }

    record Dont() implements Instruction {
    }
  }

  private Parser<String, Integer> instruction() {
    return tag("mul").and(
        delimited(tag("("), separatedPair(number(), tag(","), number()), tag(")"))
            .map(p -> p.first() * p.second()))
        .map(Pair::second);
  }

  private Parser<String, Integer> instructionParser() {
    return alt(instruction(), anyChar().mapValue(0)).fold(0, Integer::sum);
  }

  private int mapFound(MatchResult m) {
    String match = m.group();
    if (match.equals("do()")) {
      active = true;
      return 0;
    }

    if (match.equals("don't()")) {
      active = false;
      return 0;
    }

    return active ? Integer.parseInt(m.group(1)) * Integer.parseInt(m.group(2)) : 0;
  }
}
