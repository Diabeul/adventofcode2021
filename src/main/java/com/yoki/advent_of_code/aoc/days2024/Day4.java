package com.yoki.advent_of_code.aoc.days2024;

import java.io.IOException;
import java.io.PrintStream;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.CollectionUtil;
import com.yoki.advent_of_code.utils.StringUtil;

public class Day4 extends AocDay {

  private static final int[][] DIRECTIONS = {
      { -1, -1 }, { -1, 0 }, { -1, 1 }, { 0, -1 }, { 0, 1 }, { 1, -1 }, { 1, 0 }, { 1, 1 }
  };

  private char[][] grid;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day4(String input, PrintStream output) throws IOException {
    super(input, output);
    this.grid = StringUtil.loadMapChar(input);
  }

  public String part1() {
    return String.valueOf(findOccurenceOfWord("XMAS"));
  }

  public String part2() {
    char[][] word = {
        { 'M', '\0', 'S' },
        { '\0', 'A', '\0' },
        { 'M', '\0', 'S' },
    };

    return String.valueOf(find2DWord(word));
  }

  private int findOccurenceOfWord(String word) {
    int size = grid.length;
    var res = 0;
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        for (var dir : DIRECTIONS) {

          int nexti = i + (word.length() - 1) * dir[0];
          int nextj = j + (word.length() - 1) * dir[1];
          if (inRange(nexti, size) && inRange(nextj, size) &&
              matchWord(grid, i, j, dir[0], dir[1], word)) {
            res++;
          }
        }
      }
    }
    return res;
  }

  private int find2DWord(char[][] word) {
    int size = grid.length;
    int len = word.length;

    int sum = 0;
    for (int i = 0; i <= size - len; i++) {
      for (int j = 0; j <= size - len; j++) {
        var rotWord = word;
        for (int rot = 0; rot < 4; rot++) {
          rotWord = CollectionUtil.rot90(rotWord);
          if (matchWord(grid, i, j, rotWord)) {
            sum++;
            continue;
          }
        }
      }
    }
    return sum;
  }

  private boolean matchWord(char[][] grid, int i, int j, char[][] word) {
    for (int x = 0; x < word.length; x++) {
      for (int y = 0; y < word[0].length; y++) {
        int dx = i + x;
        int dy = j + y;
        if (word[x][y] != '\0' && grid[dx][dy] != word[x][y]) {
          return false;
        }
      }
    }

    return true;
  }

  private boolean matchWord(char[][] grid, int i, int j, int x, int y, String word) {
    for (int c = 0; c < word.length(); c++) {
      int ni = i + c * x;
      int nj = j + c * y;
      if (grid[ni][nj] != word.charAt(c))
        return false;
    }
    return true;
  }

  private boolean inRange(int nxt, int size) {
    return 0 <= nxt && nxt < size;
  }
}
