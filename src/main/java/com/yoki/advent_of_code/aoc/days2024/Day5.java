package com.yoki.advent_of_code.aoc.days2024;

import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.CollectionUtil;

public class Day5 extends AocDay {

  private static PageRules rules = new PageRules();
  private List<Update> updates;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day5(String input, PrintStream output) {
    super(input, output);
    input.lines()
        .filter(l -> l.contains("|"))
        .forEach(rules::addRule);

    this.updates = input.lines()
        .filter(l -> l.contains(","))
        .map(Update::valueOf).toList();

    // sortedRules = rules.kahnTopologicalSort();
    // sortedRules = rules.dfsTopologicalSort();
  }

  public String part1() {
    return String.valueOf(this.updates.stream()
        .filter(Update::valid)
        .mapToInt(Update::middle)
        .sum());
  }

  public String part2() {
    return String.valueOf(this.updates.stream()
        .filter(Predicate.not(Update::valid))
        .map(Update::fix)
        .mapToInt(Update::middle)
        .sum());
  }

  private record Update(List<Integer> updates) {
    public static Update valueOf(String l) {
      return new Update(Arrays.stream(l.split(","))
          .map(Integer::valueOf)
          .collect(Collectors.toList()));
    }

    public Update fix() {
      for (int i = 0; i < updates.size(); i++) {
        for (int j = 0; j < i; j++) {
          if (rules.getRule(updates.get(i)).contains(updates.get(j))) {
            updates.add(j, updates.get(i));
            updates.remove(i + 1);
            i = 0;
          }
        }
      }

      return new Update(updates);
    }

    public boolean valid() {
      Set<Integer> seen = new HashSet<>();
      for (var u : updates.reversed()) {
        if (!rules.matchRule(u, seen)) {
          return false;
        }
        seen.add(u);
      }
      return true;
    }

    public Integer middle() {
      int mid = this.updates().size() / 2;
      return this.updates().get(mid);
    }
  }

  private record PageRules(Map<Integer, Set<Integer>> rules) {
    public PageRules() {
      this(new HashMap<>());
    }

    public void addRule(String line) {
      var splitted = line.split("\\|", 2);
      int page = Integer.parseInt(splitted[0]);
      int before = Integer.parseInt(splitted[1]);

      rules.computeIfAbsent(page, _ -> new HashSet<>()).add(before);
    }

    public boolean matchRule(Integer val, Set<Integer> seen) {
      var all = this.rules.getOrDefault(val, Set.of());
      return seen.stream().allMatch(all::contains);
    }

    public Set<Integer> getRule(Integer val) {
      return this.rules.getOrDefault(val, Set.of());
    }

    /**
     * Actually a stupid bubble sort would work
     */
    public List<Integer> dfsTopologicalSort() {
      List<Integer> sorted = new ArrayList<>();

      // 0 = unvisited, 1 = visiting, 2 = visited
      Map<Integer, Integer> visited = rules.keySet().stream()
          .collect(Collectors.toMap(k -> k, _ -> 0));

      System.out.println(visited);

      for (Integer node : this.rules.keySet()) {
        if (visited.get(node) == 0) {
          if (!dfsVisit(node, visited, sorted)) {
            throw new IllegalStateException("Graph contains a cycle.");
          }
        }
      }

      Collections.reverse(sorted);

      return sorted;
    }

    private boolean dfsVisit(int node, Map<Integer, Integer> visited, List<Integer> sorted) {
      visited.put(node, 1); // mark as "visiting"

      for (int n : this.getRule(node)) {
        visited.putIfAbsent(n, 0);
        if (visited.get(n) == 1)
          return false;
        if (visited.get(n) == 0) {
          if (!dfsVisit(n, visited, sorted))
            return false;
        }
      }

      visited.put(node, 2);
      sorted.add(node);
      return true;
    }

    /**
     * Tried with kahn but because there are no parent node it doesn't work
     **/
    public List<Integer> kahnTopologicalSort() {

      Map<Integer, Integer> inDegree = new HashMap<>();
      for (var entry : this.rules.entrySet()) {
        inDegree.putIfAbsent(entry.getKey(), 0);
        entry.getValue().forEach(n -> inDegree.put(n, inDegree.getOrDefault(n, 0) + 1));
      }
      System.out.println("InDegree: " + inDegree);

      var parents = inDegree.entrySet().stream()
          .filter(e -> e.getValue() == 0)
          .map(Entry::getKey)
          .toList();

      System.out.println("Parents: " + parents);

      Deque<Integer> toVisit = new ArrayDeque<>(parents);
      List<Integer> sorted = new ArrayList<>();
      while (!toVisit.isEmpty()) {
        int node = toVisit.poll();
        sorted.add(node);

        for (int n : this.getRule(node)) {
          inDegree.put(n, inDegree.get(n) - 1);
          if (inDegree.get(n) == 0) {
            toVisit.add(n);
          }
        }

      }

      return sorted;
    }

  }

}
