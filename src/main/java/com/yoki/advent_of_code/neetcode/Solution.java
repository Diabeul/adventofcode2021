package com.yoki.advent_of_code.neetcode;

import com.yoki.advent_of_code.utils.data_structure.Counter;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Solution {

  public static void main(String[] args) {
    System.out.println(isAnagram("racecar", "carrace"));
    System.out.println(isAnagram("jar", "jam"));
    System.out.println(isAnagram("bbcc", "ccbc"));

    System.out.println("---");

    System.out.println(isAnagram2("racecar", "carrace"));
    System.out.println(isAnagram2("jar", "jam"));
    System.out.println(isAnagram2("bbcc", "ccbc"));
  }

  /**
   * Given an integer array nums, return true if any value appears more than once in the array, otherwise return false.
   */
  public static boolean hasDuplicate(int[] nums) {
    Set<Integer> seen = new HashSet<>();
    return IntStream.of(nums).allMatch(seen::add);
  }

  public static boolean isAnagram(String s, String t) {
    if (s.length() != t.length()) return false;

    Counter<Character> characterCounter = new Counter<>(s.chars()
        .mapToObj(s1 -> Character.valueOf((char) s1))
        .collect(Collectors.toList()));
    for (char c : s.toCharArray()) {

    }
    return true;
  }

  public static boolean isAnagram2(String s, String t) {
    if (s.length() != t.length()) return false;
    return s.chars().allMatch(c -> t.contains((char) c + ""));
  }
}
