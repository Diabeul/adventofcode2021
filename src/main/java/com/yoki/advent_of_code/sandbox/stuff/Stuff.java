package com.yoki.advent_of_code.sandbox.stuff;

import java.util.Optional;

public class Stuff {

    public static void main(String[] args) {
        TNode node = new TNode(5, Optional.empty(), Optional.empty());
        TNode node2 = new TNode(5, Optional.empty(), Optional.empty());

        System.out.println(compare(Optional.of(node), Optional.of(node2)));
    }

    private record TNode(int value, Optional<TNode> left, Optional<TNode> right){

    }

    private static boolean compare(Optional<TNode> a, Optional<TNode> b) {
       if (a.isEmpty() && b.isEmpty()) return true;
       if (a.isEmpty() || b.isEmpty()) return false;

       TNode na = a.get();
       TNode nb = b.get();
       return na.value() == nb.value() && compare(na.left(), nb.left()) && compare(na.right(), nb.right());
    }

}
