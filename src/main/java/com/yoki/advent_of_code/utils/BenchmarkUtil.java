package com.yoki.advent_of_code.utils;

import static java.lang.System.currentTimeMillis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.function.Predicate;

public class BenchmarkUtil {

  private BenchmarkUtil() {}

  public static void main(String[] args) {
    String input = "pnnfhnhshrhmhw";
    printBench(input, BenchmarkUtil::allUniqueDistinct, BenchmarkUtil::allUniqueHashset, BenchmarkUtil::allUniqueList,
        BenchmarkUtil::allUniqueArray);
  }

  public static void printBench(String input, Predicate<String>... ps) {
    long t;
    long[] ts = new long[ps.length];
    for (int i = 0; i < 10000; i++) {
      for (int j = 0; j < ps.length; j++) {
        t = currentTimeMillis();
        ps[j].test(input);
        ts[j] += currentTimeMillis() - t;
      }
    }
    Arrays.stream(ts).forEach(time -> System.out.println("Method took: " + time + " ms"));
  }

  private static boolean allUniqueDistinct(String substring) {
    return substring.chars().distinct().count() == substring.length();
  }

  private static boolean allUniqueHashset(String substring) {
    HashSet<Character> characters = new HashSet<>();
    for (int i = 0; i < substring.length(); i++)
      if (!characters.add(substring.charAt(i)))
        return false;
    return true;
  }

  private static boolean allUniqueList(String substring) {
    List<Character> characters = new ArrayList<>();
    for (int i = 0; i < substring.length(); i++) {
      char c = substring.charAt(i);
      if (characters.contains(c))
        return false;
      characters.add(c);
    }
    return true;
  }

  private static boolean allUniqueArray(String substring) {
    char[] chars = new char[14];
    for (int i = 0; i < substring.length(); i++) {
      char c = substring.charAt(i);
      for (int j = 0; j < i; j++) {
        if (chars[j] == c)
          return false;
      }
      chars[i] = c;
    }
    return true;
  }

}
