package com.yoki.advent_of_code.utils;

public class Constants {
  public static final int[][] DIRECTIONS = {
      { -1, -1 }, { -1, 0 }, { -1, 1 }, { 0, -1 }, { 0, 1 }, { 1, -1 }, { 1, 0 }, { 1, 1 }
  };
}
