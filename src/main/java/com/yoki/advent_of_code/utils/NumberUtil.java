package com.yoki.advent_of_code.utils;

public class NumberUtil {
  private NumberUtil() {}

  public static int mostSignificantDigit(int n) {
    double k = Math.log10(n);
    k -= Math.floor(k);
    return (int)Math.pow(10, k);
  }

  public static int numberOfDigits(int n) {
    return (int) (Math.floor(Math.log10(n)) + 1);
  }
}
