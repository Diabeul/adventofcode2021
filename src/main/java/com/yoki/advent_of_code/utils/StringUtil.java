package com.yoki.advent_of_code.utils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import lombok.NonNull;
import one.util.streamex.IntStreamEx;

public class StringUtil {

  public static final List<String> ALPHA = Arrays
      .stream("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".split("")).toList();
  public static final String LIGHT = Character.toString(0x2593);
  public static final String DARK = Character.toString(0x2591);

  private StringUtil() {
  }

  public static String order(String s) {
    char[] chars = s.toCharArray();
    Arrays.sort(chars);
    return new String(chars);
  }

  public static List<Character> stringToChars(@NonNull String s) {
    return IntStreamEx.ofChars(s).mapToObj(c -> (char) c).toList();
  }

  public static String replaceAt(String str, String in, String out, int position) {
    return str.substring(0, position) + out + str.substring(position + in.length());
  }

  public static List<String> splitInHalf(String s) {
    int mid = s.length() / 2;
    return List.of(s.substring(0, mid), s.substring(mid));
  }

  public static String replaceNthOccurrence(String str, String toReplace, String replacement, int n) {
    StringBuilder res = new StringBuilder();
    int count = 0;
    for (int i = 0; i < str.length(); i++) {
      String s = String.valueOf(str.charAt(i));
      if (str.startsWith(toReplace, i)) {
        if (count == n) {
          s = replacement;
          i += toReplace.length() - 1;
        }
        count++;
      }
      res.append(s);
    }
    return res.toString();
  }

  public static char[][] loadMapChar(String input) {
    var outputValues = input.lines()
        .map(l -> l.chars().mapToObj(i -> (char) i).toList())
        .toList();

    int size = outputValues.size();
    int size2 = outputValues.get(0).size();
    var map = new char[size][size2];
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size2; j++) {
        map[i][j] = outputValues.get(i).get(j);
      }
    }
    return map;
  }
}
