package com.yoki.advent_of_code.utils.algo;

import com.yoki.advent_of_code.utils.vector.Tuple;
import com.yoki.advent_of_code.utils.vector.Vector;
import java.util.ArrayList;
import java.util.List;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.tuple.Pair;

public class Matrix extends Tuple<Vector> {

  public Matrix(double... el) {
    super(IntStreamEx.range((int) Math.sqrt(el.length)).mapToObj(i -> new Vector(IntStreamEx.range((int) Math.sqrt(el.length))
        .mapToObj(j -> el[(int) (i * Math.sqrt(el.length) + j)]).toList())).toList());
  }

  public Matrix(List<Vector> args) {
    super(args);
  }

  public Matrix id() {
    return Matrix.ID(elems.size());
  }

  public static Matrix ID(int size) {
    List<Vector> vs = new ArrayList<>();
    for (int i = 0; i < size; i++) {
      List<Double> ints = new ArrayList<>();
      for (int j = 0; j < size; j++) {
        ints.add(i==j ? 1d : 0d);
      }
      vs.add(new Vector(ints));
    }
    return new Matrix(vs);
  }

  private Vector apply(Vector vec) {
    return new Vector(StreamEx.of(zip()).map(v ->
        StreamEx.zip(v.zip(), vec.zip(), Pair::of).mapToDouble(p -> p.getLeft() * p.getRight()).sum()
    ).toList());
  }

  private Matrix multiply(Matrix mat) {
    List<Vector> zip1 = zip(mat);
    Matrix o = new Matrix(StreamEx.of(zip1).map(this::apply).toList());
    List<Vector> zip = zip(o);
    return new Matrix(zip);
  }

  public Vector mul(Vector other) {
    return apply(other);
  }

  public Matrix mul(Matrix other) {
    return multiply(other);
  }

  public Matrix mul(double n) {
    return new Matrix(StreamEx.of(zip()).map(e -> e.mul(n)).toList());
  }

  public Matrix pow(int exp) {
    if (exp < 0 || exp > 99)
      throw new Error("Exponent must be in range [0,100[");
    if (exp == 0)
      return id();

    var res = new Matrix(zip());
    for (int i = 0; i < exp - 1; i++) {
      res = res.mul(this);
    }
    return res;
  }

  private List<Vector> zip(Matrix o) {
    int size = this.elems.size();
    List<Vector> vectors = new ArrayList<>();
    for (int i = 0; i < size; i++) {
      List<Double> ints = new ArrayList<>();
      for (int j = 0; j < size; j++) {
        ints.add(o.get(j).zip().get(i));
      }
      vectors.add(new Vector(ints));
    }
    return vectors;
  }

  @Override
  public String toString() {
    return StreamEx.of(elems).map(e -> "[" + e.toString() + "]").joining(",\n") + "\n";
  }
}
