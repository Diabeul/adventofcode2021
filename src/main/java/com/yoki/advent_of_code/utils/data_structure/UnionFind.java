package com.yoki.advent_of_code.utils.data_structure;

import java.util.Arrays;

public class UnionFind {

  private final int[] parents;
  private final int[] size;

  public UnionFind(int n) {
    parents = new int[n + 1];
    size = new int[n + 1];
    for (int i = 0; i <= n; i++) {
      parents[i] = i;
      size[i] = 0;
    }
  }

  public int find(int x) {
    return parents[x] == x ? x : (parents[x] = find(parents[x]));
  }

  public void union(int x, int y) {
    int fx = find(x);
    int fy = find(y);
    parents[fy] = fx;
  }

  public void unionSized(int x, int y) {
    int fx = find(x);
    int fy = find(y);
    if (size[fx] < size[fy]) {
      parents[fx] = fy;
      size[fy]++;
    } else {
      parents[fy] = fx;
      size[fx]++;
    }
  }

  @Override
  public String toString() {
    return "UnionFind{" +
        "parents=" + Arrays.toString(parents) +
        ", size=" + Arrays.toString(size) +
        '}';
  }
}
