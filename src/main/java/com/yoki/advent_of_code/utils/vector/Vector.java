package com.yoki.advent_of_code.utils.vector;

import static java.lang.Math.abs;

import java.util.List;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.tuple.Pair;

public class Vector extends Tuple<Double> {

  public Vector(Double... toList) {
    super(toList);
  }

  public Vector(List<Double> toList) {
    super(toList);
  }

  public Vector sub(Vector other) {
    return new Vector(StreamEx.zip(this.zip(), other.zip(), Pair::of).map(p -> p.getLeft() - p.getRight()).toList());
  }

  public Vector add(Vector other) {
    return new Vector(StreamEx.zip(this.zip(), other.zip(), Pair::of).map(p -> p.getLeft() + p.getRight()).toList());
  }

  public Vector mul(double n) {
    return new Vector(StreamEx.of(this.zip()).map(v -> v.intValue() * n).toList());
  }

  public static double manhattanDistance(Vector v1, Vector v2) {
    if (v1.size() != v2.size())
      throw new Error("Vectors size doesn't match");

    //  return abs(v1.get(0) - v2.get(0)) + abs(v1.get(1) - v2.get(1)) + abs(v1.get(2) - v2.get(2));
    return IntStreamEx.range(v1.size())
        .mapToDouble(i -> abs(v1.get(i) - v2.get(i)))
        .sum();
  }
}
