package com.yoki.advent_of_code.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import org.junit.jupiter.api.Test;


class CollectionUtilTest {

  @Test
  void testZip() {
    List<Integer> a = List.of(1, 2, 3);
    List<Integer> b = List.of(1, 2, 3, 4);
    List<Integer> list = CollectionUtil.zip(a.stream(), b.stream(), Integer::sum).toList();
    assertTrue(list.size() == 3);
  }
}