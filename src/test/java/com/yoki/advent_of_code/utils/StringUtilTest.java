package com.yoki.advent_of_code.utils;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

class StringUtilTest {

  record UTest(Object expected, Object... input) {}

  @TestFactory
  Stream<DynamicTest> testStringToChars() {
    List<UTest> data = List.of(
        new UTest(List.of('a', 'b', 'c', 'd', 'e'), "abcde"),
        new UTest(List.of(), "")
    );

    return data.stream().map(test ->
        dynamicTest(Arrays.toString(test.input) + " -> " + test.expected,
            () -> assertEquals(test.expected, StringUtil.stringToChars((String) test.input[0]))));
  }

  @TestFactory
  Stream<DynamicTest> testReplaceNthOccurrence() {
    List<UTest> data = List.of(
        new UTest("zbcadea", "abcadea", "a", "z", 0),
        new UTest("abczdea", "abcadea", "a", "z", 1),
        new UTest("aabcaadez", "aabcaadeaa", "aa", "z", 2),
        new UTest("aabcaadeaa", "aabcaadeaa", "aa", "z", 3),
        new UTest("aaabczbf", "aaabcaaaabf", "aaaa", "z", 0)
    );

    return data.stream().map(test ->
        dynamicTest(Arrays.toString(test.input) + " -> " + test.expected,
            () -> assertEquals(test.expected,
                StringUtil.replaceNthOccurrence((String) test.input[0], (String)test.input[1], (String)test.input[2], (int)test.input[3]))));
  }
}